# Assignment3_FV

## Supporti utilizzati
* Build Automation System: Maven
* IDE: Eclipse
* Database: MySQL

## Setup ed esecuzione del progetto

Per poter eseguire il progetto è necessario fare prima il setup, in ordine bisogna:
* preparare l’ambiente andando ad installare l’IDE consigliato, Eclipse, e il database MySQL;
* fare il clone della repository https://gitlab.com/Vita1996/assignment3_fv;
* importare il progetto di tipo Maven, con un IDE compatibile
* andare a modificare il file src/main/resources/hibernate.cfg.xml settando le proprie credenziali, username e password, per l’accesso al database;
* avviare il progetto, in particolare
	* il file src/main/java/assignment3/entities/main.java per eseguire il progetto ed utilizzare l’interfaccia a riga di comando;
	* il file src/main/java/test/executeTest.java per eseguire tutti i test.


### La procedura di importazione è la seguente
* da Eclipse fare File e poi Import
![](img/import_project.png)
* fare next e poi browse fino a posizionarsi all’interno della cartella creata con il clone
![](img/import_maven_path.png)
* infine cliccare su finish ed il progetto è importato correttamente.
