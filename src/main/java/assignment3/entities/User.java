package assignment3.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table
public class User {

	@Id
	private String username;
	
	private String password;
	
	private Date registration_date;

	public User() {}

	public User(String username, String password, Date registration_date) {
		this.username = username;
		this.password = password;
		this.registration_date = registration_date;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getRegistrationDate() {
		return registration_date;
	}
	public void setRegistrationDate(Date registration_date) {
		this.registration_date = registration_date;
	}
	
	public String getRegistrationDateString() {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(this.registration_date);
	}
	
	@Override
	public String toString() {
		return "User Details = Username: " + this.username + ", Password: " + this.password + ", Registration Date: " + this.registration_date;
	}
}