package assignment3.entities;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table
public class Director extends Person{
	
	private int film_directed;
	
	public Director() {
		super();
	}
		
	public Director(String firstname, String lastname, Date birthdate, Gender gender, int film_directed) {
		super(firstname, lastname, birthdate, gender);
		this.film_directed = film_directed;
	}
	
	public int getFilm_directed() {
		return film_directed;
	}
	public void setFilm_directed(int n_film) {
		film_directed = n_film;
	}

	public String fullName() {
		return super.fullName();
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Number film directed: " + this.film_directed;
	}
}