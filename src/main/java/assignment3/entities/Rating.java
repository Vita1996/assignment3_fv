package assignment3.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table
public class Rating {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	private int id;
	
	private float vote;
	
	private Date date_vote;
	
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="movie_id")
    private Movie movie;
	
	public Rating() {}
	
	public Rating(Date date_vote, String comment) {
		this.date_vote = date_vote;
		this.comment = comment;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public float getVote() {
		return vote;
	}

	public void setVote(float vote) {
		this.vote = vote;
	}

	public Date getDateVote() {
		return date_vote;
	}
	public void setDateVote(Date date_vote) {
		this.date_vote = date_vote;
	}
	public String getDateVoteString() {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(this.date_vote);
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@Override
	public String toString() {
		String rating_details = "\nRating details = Id: " + this.id + ", Date Vote: " + this.date_vote + ", Comment: " + this.comment;
	
		if(user != null) 
			rating_details += (", User: " + user.getUsername());
		
		if(movie != null) 
			rating_details += (", Movie: " + movie.getTitle());
		
		return rating_details;
	}
}