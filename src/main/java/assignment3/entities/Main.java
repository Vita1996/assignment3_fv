package assignment3.entities;

import java.text.ParseException;
import java.util.Scanner;

import org.apache.log4j.Logger;

import assignment3.dao.*;
import assignment3.interfaces.*;

public class Main {
	public final static Logger logger = Logger.getLogger(Main.class);
	static Scanner reader = new Scanner(System.in);

	public static void printOperationMenu () {
		System.out.println("\n======= MENU' =======");
		System.out.println("Choose from the following menu which option you want\n");
		System.out.println("1. Create new record");
		System.out.println("2. Retrieve all records of a table");
		System.out.println("3. Update an existing record");
		System.out.println("4. Delete a record");
		System.out.println("5. Delete all records of a table");
		System.out.println("6. Find a record");
		System.out.println("7. Empty the whole database");
		System.out.println("8. Populate with a small default database");
		System.out.println("9. Exit");
		System.out.println("Enter the number of the option you want: ");
	}
	
	public static void printTableMenu () {
		System.out.println("Choose from the following new menu which table you want to alter\n");
		System.out.println("1. Movie");
		System.out.println("2. Actor");
		System.out.println("3. Director");
		System.out.println("4. Genre");
		System.out.println("5. Studio");
		System.out.println("6. Rating");
		System.out.println("7. User");
		System.out.println("8. Back");
		System.out.println("Enter the number of the option you want: ");
	}
	
	public static void main(String[] args) throws ParseException {
		logger.info("....... Hibernate Crud Operations .......\n");
		
		//MENU
		logger.info("\n======= CREATE DATABASE =======\n");
		EntityService.createDatabase();
		
		int op_option, entity_option = 0;
		
		while(true)
		{
			do{
				printOperationMenu();
				op_option = reader.nextInt();
				if(op_option<1 || op_option>9)
					System.out.println("Value inserted not valid!");
			} while(op_option<1 || op_option>9);
			
			switch(op_option) {
				// Create
				case 1:
					do{
						do{
							System.out.println("\n======= CREATION MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieInterface.createMovie();
								break;
								
							case 2:
								ActorInterface.createActor();
								break;
								
							case 3:
								DirectorInterface.createDirector();
								break;
								
							case 4:
								GenreInterface.createGenre();
								break;
								
							case 5:
								StudioInterface.createStudio();
								break;
								
							case 6:
								RatingInterface.createRating();
								break;
								
							case 7:
								UserInterface.createUser();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Retrieve
				case 2:
					do{
						do{
							System.out.println("\n======= RETRIVE MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieDao.retriveMovie();
								break;
								
							case 2:
								ActorDao.retriveActor();
								break;
								
							case 3:
								DirectorDao.retriveDirector();
								break;
								
							case 4:
								GenreDao.retriveGenre();
								break;
								
							case 5:
								StudioDao.retriveStudio();
								break;
								
							case 6:
								RatingDao.retriveRating();
								break;
								
							case 7:
								UserDao.retriveUser();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Update
				case 3:
					do{
						do{
							System.out.println("\n======= UPDATE MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieInterface.updateMovie();
								break;
								
							case 2:
								ActorInterface.updateActor();
								break;
								
							case 3:
								DirectorInterface.updateDirector();
								break;
								
							case 4:
								GenreInterface.updateGenre();
								break;
								
							case 5:
								StudioInterface.updateStudio();
								break;
								
							case 6:
								RatingInterface.updateRating();
								break;
								
							case 7:
								UserInterface.updateUser();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Delete
				case 4:
					do{
						do{
							System.out.println("\n======= DELETE MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieInterface.deleteMovie();
								break;
								
							case 2:
								ActorInterface.deleteActor();
								break;
								
							case 3:
								DirectorInterface.deleteDirector();
								break;
								
							case 4:
								GenreInterface.deleteGenre();
								break;
								
							case 5:
								StudioInterface.deleteStudio();
								break;
								
							case 6:
								RatingInterface.deleteRating();
								break;
								
							case 7:
								UserInterface.deleteUser();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Delete All
				case 5:
					do{
						do{
							System.out.println("\n======= DELETE ALL MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieDao.deleteAllMovies();
								break;
								
							case 2:
								ActorDao.deleteAllActors();
								break;
								
							case 3:
								DirectorDao.deleteAllDirectors();
								break;
								
							case 4:
								GenreDao.deleteAllGenres();
								break;
								
							case 5:
								StudioDao.deleteAllStudios();
								break;
								
							case 6:
								RatingDao.deleteAllRatings();
								break;
								
							case 7:
								UserDao.deleteAllUsers();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Find
				case 6:
					do{
						do{
							System.out.println("\n======= FIND MENU' =======");
							printTableMenu();
							entity_option = reader.nextInt();
							if(entity_option<1 || entity_option>8)
								System.out.println("Value inserted not valid!");
						} while(entity_option<1 || entity_option>8);
						switch(entity_option) {
							case 1:
								MovieInterface.findMovie();
								break;
								
							case 2:
								ActorInterface.findActor();
								break;
								
							case 3:
								DirectorInterface.findDirector();
								break;
								
							case 4:
								GenreInterface.findGenre();
								break;
								
							case 5:
								StudioInterface.findStudio();
								break;
								
							case 6:
								RatingInterface.findRating();
								break;
								
							case 7:
								UserInterface.findUser();
								break;
								
							default:
								System.out.println("\nYou have chosen to go back to first menu!\n");
								break;
						}
					} while (entity_option != 8);
					break;
					
				// Empty
				case 7:
					EntityService.emptyDB();
					break;
				
				// Populate
				case 8:
					EntityService.populateDB();
					break;
					
				default:
					System.out.println("\nYou have chosen to exit, bye!\n");
					System.exit(0);
					break;
			}
		}
	} 
}
