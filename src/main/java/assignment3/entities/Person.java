package assignment3.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
@Table
public class Person{
	
	public enum Gender {MALE, FEMALE};

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	private int id;
	
	private String firstname;
	
	private String lastname;
	
	private Date birthdate;
	
	@Enumerated(EnumType.STRING)
    private Gender gender;
	
	public Person() {}
	
	public Person(String firstname, String lastname, Date birthdate, Gender gender) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.gender = gender;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String name) {
		firstname = name;
	}
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String name) {
		lastname = name;
	}
	
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date date) {
		birthdate = date;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public String fullName() {
		return this.firstname + " " + this.lastname;
	}
	
	public String getBirthdateString() {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(this.birthdate);
	}
	
	@Override
	public String toString() {
		return "Person details = Id: " + this.id + ", Firstname: " + this.firstname + ", Lastname: " + this.lastname + ", Birthdate: " + this.birthdate + ", Gender: " + this.gender;
	}
}
