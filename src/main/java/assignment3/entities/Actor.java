package assignment3.entities;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table
public class Actor extends Person{

	private int film_acted;

	public Actor() {
		super();
	}
	
	public Actor(String firstname, String lastname, Date birthdate, Gender gender, int film_acted) {
		super(firstname, lastname, birthdate, gender);
		this.film_acted = film_acted;
	}
	
	public int getFilm_acted() {
		return film_acted;
	}
	public void setFilm_acted(int n_film) {
		film_acted = n_film;
	}

	public String fullName() {
		return super.fullName();
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Number film acted: " + this.film_acted;
	}
}
