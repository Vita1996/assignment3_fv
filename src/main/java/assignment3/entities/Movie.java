package assignment3.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table
public class Movie {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	private int id;
	
	private String title;
	
	private int runtime;
	
	private Date release_date;
	
	private String overview;
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(
	            name = "movie_genre",
	            joinColumns = @JoinColumn(name = "movie_id"),
	            inverseJoinColumns = @JoinColumn(name = "genre_id")
	    )
	private Set<Genre> genres = new HashSet<Genre>();
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(
	            name = "movie_actor",
	            joinColumns = @JoinColumn(name = "movie_id"),
	            inverseJoinColumns = @JoinColumn(name = "actor_id")
	    )
	private Set<Actor> actors = new HashSet<Actor>();
	
	@ManyToOne
    private Director director;
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(
	            name = "movie_sequel",
	            joinColumns = @JoinColumn(name = "prequel_id"),
	            inverseJoinColumns = @JoinColumn(name = "sequel_id")
	    )
	@LazyCollection(LazyCollectionOption.FALSE)
    private Set<Movie> sequels = new HashSet<Movie>();
	
	@ManyToOne
    private Studio studio;

	public Movie() {}
	
	public Movie(String title, int runtime, Date release_date, String overview) {
		this.title = title;
		this.runtime = runtime;
		this.release_date = release_date;
		this.overview = overview;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	
	public Date getReleaseDate() {
		return release_date;
	}
	public void setReleaseDate(Date release_date) {
		this.release_date = release_date;
	}
	
	public String getReleaseDateString() {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(this.release_date);
	}
	
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	
	public Set<Genre> getGenres() {
		return genres;
	}
	public void setGenres(Genre genre_record) {
		genres.add(genre_record);
	}
	
	public Studio getStudio() {
		return studio;
	}
	public void setStudio(Studio studio) {
		this.studio = studio;
	}
	
	public Set<Actor> getActors() {
		return actors;
	}
	public void setActors(Actor actor_record) {
		actors.add(actor_record);
	}
	
	public Director getDirector() {
		return director;
	}
	public void setDirector(Director director) {
		this.director = director;
	}
	
	public Set<Movie> getSequels() {
		return sequels;
	}
	public void setSequels(Movie movie_record) {
		sequels.add(movie_record);
	}
	
	@Override
	public String toString() {
		String movie_details = "\nMovie details = Id: " + this.id + ", Title: " + this.title + ", Runtime: " + this.runtime + ", Release Date: " + this.release_date + ", Overview: " + this.overview;
		
		if(genres.size()>0) {
			movie_details += ", Genres: {";
			for(Genre record : genres)
				movie_details += (record.getName() + ", ");
			movie_details = movie_details.substring(0, movie_details.length() - 2); // to remove the last space and comma
			movie_details += "}";
		}
		
		if(studio != null) 
			movie_details += (", Studio: " + studio.getName());
		
		if(actors.size()>0) {
			movie_details += ", Actors: {";
			for(Actor record : actors)
				movie_details += (record.fullName() + ", ");
			movie_details = movie_details.substring(0, movie_details.length() - 2); // to remove the last space and comma
			movie_details += "}";
		}
		
		if(director != null) 
			movie_details += (", Director: " + director.fullName());
		
		if(sequels.size()>0) {
			movie_details += ", Related Movie: {";
			for(Movie record : sequels)
				movie_details += (record.getTitle() + ", ");
			movie_details = movie_details.substring(0, movie_details.length() - 2); // to remove the last space and comma
			movie_details += "}";
		}
		
		return movie_details;
	}
}