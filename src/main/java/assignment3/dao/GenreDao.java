package assignment3.dao;

import java.util.List;

import assignment3.entities.Genre;

public class GenreDao {
	
	public GenreDao() {}
	
	public static int createGenre(String name,  String description) {
		
		Genre genre = new Genre();
		genre.setName(name);
		genre.setDescription(description);
		
		return EntityService.createRecord(genre);
	}
	
	public static void retriveGenre() {
		
		List<Object> genre_records = EntityService.displayRecords("Genre");
		
		if(genre_records != null && genre_records.size()>0) {
			
			System.out.println("\nThere are " + genre_records.size() + " records in the Genre table:");
			for(Object record : genre_records)
				System.out.println(((Genre) record).toString());
		}
		else
			System.out.println("\nThe Genre table is still empty!");
	}
	
	public static void updateGenre(int id, String new_name, String new_description) {
		
		Genre update_genre = new Genre();
		
		update_genre.setId(id);
		update_genre.setName(new_name);
		update_genre.setDescription(new_description);
			
		EntityService.updateRecord(update_genre);
	}
	
	public static void deleteGenre(int id) {
		
		Genre deleted_genre = new Genre();
		deleted_genre.setId(id);
		
		EntityService.deleteRecord(deleted_genre);
	}
	
	public static void deleteAllGenres() {
		
		EntityService.deleteAllRecord("Genre");
	}
	
	public static Genre findGenre(int id) {
		
		Genre get_genre = new Genre();
		
		get_genre = (Genre) EntityService.findRecord("Genre", id);
		if(get_genre != null)
			System.out.println(get_genre.toString());
		
		return get_genre;
	}
}