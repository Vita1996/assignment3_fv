package assignment3.dao;

import java.util.Date;
import java.util.List;

import assignment3.entities.Rating;
import assignment3.entities.User;
import assignment3.entities.Movie;

public class RatingDao {
	
	public RatingDao() {}
	
	public static int createRating(User user, Movie movie, float vote, Date date_vote, String comment) {
		
		Rating rating = new Rating();
		rating.setUser(user);
		rating.setMovie(movie);
		rating.setVote(vote);
		rating.setDateVote(date_vote);
		rating.setComment(comment);
		
		return EntityService.createRecord(rating);
	}
	
	public static void retriveRating() {
		
		List<Object> rating_records = EntityService.displayRecords("Rating");
		
		if(rating_records != null && rating_records.size()>0) {
			
			System.out.println("\nThere are " + rating_records.size() + " records in the Rating table:");
			for(Object record : rating_records)
				System.out.println(((Rating) record).toString());
		}
		else
			System.out.println("\nThe Rating table is still empty!");
	}
	
	public static void updateRating(int id, User new_user, Movie new_movie, float new_vote, Date new_date_vote, String new_comment) {
		
		Rating update_rating = new Rating();

		update_rating.setId(id);
		update_rating.setUser(new_user);
		update_rating.setMovie(new_movie);
		update_rating.setVote(new_vote);
		update_rating.setDateVote(new_date_vote);
		update_rating.setComment(new_comment);
		
		EntityService.updateRecord(update_rating);
	}
	
	public static void deleteRating(int id) {
		
		Rating deleted_rating = new Rating();

		deleted_rating.setId(id);
		
		EntityService.deleteRecord(deleted_rating);
	}
	
	public static void deleteAllRatings() {
		
		EntityService.deleteAllRecord("Rating");
	}
	
	public static Rating findRating(int id) {
		
		Rating get_rating = new Rating();
		
		get_rating = (Rating) EntityService.findRecord("Rating", id);
		if(get_rating != null)
			System.out.println(get_rating.toString());
		
		return get_rating;
	}
}
