package assignment3.dao;

import java.util.Date;
import java.util.List;

import assignment3.entities.User;

public class UserDao {

	public UserDao() {}
	
	public static String createUser(String username, String password, Date registration_date) {
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setRegistrationDate(registration_date);
		
		return EntityService.createRecord(user, "user");
	}
	
	public static void retriveUser() {
		
		List<Object> user_records = EntityService.displayRecords("User");
		
		if(user_records != null && user_records.size()>0) {
			
			System.out.println("\nThere are " + user_records.size() + " records in the User table:");
			for(Object record : user_records)
				System.out.println(((User) record).toString());
		}
		else
			System.out.println("\nThe User table is still empty!");
	}
	
	public static void updateUser(String username, String password, Date registration_date){
		
		User update_user = new User();
		
		update_user.setUsername(username);
		update_user.setPassword(password);
		update_user.setRegistrationDate(registration_date);
			
		EntityService.updateRecord(update_user);
	}
	
	public static void deleteUser(String username) {
		
		User deleted_user = new User();
		deleted_user.setUsername(username);
		
		EntityService.deleteRecord(deleted_user);
	}
	
	public static void deleteAllUsers() {
		
		EntityService.deleteAllRecord("User");
	}
	
	public static User findUser(String username) {
		
		User get_user = new User();
				
		get_user = (User) EntityService.findRecord("User", username);
		if(get_user != null)
			System.out.println(get_user.toString());
		
		return get_user;
	}
}
