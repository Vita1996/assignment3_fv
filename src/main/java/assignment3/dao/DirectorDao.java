package assignment3.dao;

import java.util.Date;
import java.util.List;

import assignment3.entities.Director;
import assignment3.entities.Person.Gender;

public class DirectorDao {
	
	public DirectorDao() {}
	
	public static int createDirector(String firstname, String lastname, Date birthdate, Gender gender, int film_directed) {
		
		Director director = new Director();
		director.setFirstname(firstname);
		director.setLastname(lastname);
		director.setBirthdate(birthdate);
		director.setGender(gender);
		director.setFilm_directed(film_directed);
		
		return EntityService.createRecord(director);
	}
	
	public static void retriveDirector() {
		
		List<Object> director_records = EntityService.displayRecords("Director");
		
		if(director_records != null && director_records.size()>0) {
			
			System.out.println("\nThere are " + director_records.size() + " records in the Director table:");
			for(Object record : director_records)
				System.out.println(((Director) record).toString());
		}
		else
			System.out.println("\nThe Director table is still empty!");
	}
	
	public static void updateDirector(int id, String new_firstname, String new_lastname, Date new_birthdate, Gender new_gender, int new_film_directed) {
		
		Director update_director = new Director();
		
		update_director.setId(id);
		update_director.setFirstname(new_firstname);
		update_director.setLastname(new_lastname);
		update_director.setBirthdate(new_birthdate);
		update_director.setGender(new_gender);
		update_director.setFilm_directed(new_film_directed);
			
		EntityService.updateRecord(update_director);
	}
	
	public static void deleteDirector(int id) {
		
		Director deleted_director = new Director();
		
		deleted_director.setId(id);
		
		EntityService.deleteRecord(deleted_director);
	}
	
	public static void deleteAllDirectors() {
		
		EntityService.deleteAllRecord("Director");
	}
	
	public static Director findDirector(int id) {
		
		Director get_director = new Director();
		
		get_director = (Director) EntityService.findRecord("Director", id);
		if(get_director != null)
			System.out.println(get_director.toString());
		
		return get_director;
	}
}
