package assignment3.dao;

import java.util.List;

import assignment3.entities.Studio;

public class StudioDao {
	
	public StudioDao() {}
	
	public static int createStudio(String name, String nation) {
		
		Studio studio = new Studio();
		studio.setName(name);
		studio.setNation(nation);
		
		return EntityService.createRecord(studio);
	}
	
	public static void retriveStudio() {
		
		List<Object> studio_records = EntityService.displayRecords("Studio");
		
		if(studio_records != null && studio_records.size()>0) {
			
			System.out.println("\nThere are " + studio_records.size() + " records in the Studio table:");
			for(Object record : studio_records)
				System.out.println(((Studio) record).toString());
		}
		else
			System.out.println("\nThe Studio table is still empty!");
	}
	
	public static void updateStudio(int id, String new_name, String new_nation) {
		
		Studio update_studio = new Studio();
		
		update_studio.setId(id);
		update_studio.setName(new_name);
		update_studio.setNation(new_nation);
		
		EntityService.updateRecord(update_studio);
	}
	
	public static void deleteStudio(int id) {
		
		Studio deleted_studio = new Studio();
		deleted_studio.setId(id);
		
		EntityService.deleteRecord(deleted_studio);
	}
	
	public static void deleteAllStudios() {
		
		EntityService.deleteAllRecord("Studio");
	}
	
	public static Studio findStudio(int id) {
		
		Studio get_studio = new Studio();
		
		get_studio = (Studio) EntityService.findRecord("Studio", id);
		if(get_studio != null)
			System.out.println(get_studio.toString());
		
		return get_studio;
	}
}
