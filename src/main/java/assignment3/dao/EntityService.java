package assignment3.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import assignment3.entities.*;
import assignment3.entities.Person.Gender;

public class EntityService {
	static Session sessionObj;
	static SessionFactory sessionFactoryObj;
	
	public final static Logger logger = Logger.getLogger(EntityService.class);
	public static SessionFactory session_factory = new Configuration().configure().buildSessionFactory();
	
	// Method used to create the hibernate's sessionFactory object
	private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return session_factory;
        } catch (Throwable ex) {
        	logger.error("\n....... Initial SessionFactory creation failed .......\n", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
	
	// Method 0 DB_INITIALIZATION: method used to create the database structure
	public static void createDatabase() {
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			// Committing the transactions to the database
			sessionObj.getTransaction().commit();
			logger.info("\nSuccessfully database creation!\n");
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
	}
	
	// Method 1 CREATE: method used to create a new record in the database table
	public static int createRecord(Object entity_dao) {
		int id = 0;
		
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			id = (Integer) sessionObj.save(entity_dao);

			// Committing the transactions to the database
			sessionObj.getTransaction().commit();
			logger.info("\nSuccessfully created new records in the database!\n");
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
			return -1;
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
		
		return id;
	}
	
	// Method 1 CREATE: method used to create a new record in the database table
		public static String createRecord(Object entity_dao, String user) {
			String id = null;
			
			try {
				// Getting sessionOject from SessionFactory
				sessionObj = buildSessionFactory().openSession();
				// Getting transaction object from SessionObject
				sessionObj.beginTransaction();

				id = (String) sessionObj.save(entity_dao);

				// Committing the transactions to the database
				sessionObj.getTransaction().commit();
				logger.info("\nSuccessfully created new records in the database!\n");
			} catch(Exception sqlException) {
				if(sessionObj.getTransaction() != null) {
					logger.error("\n....... Transaction Is Being Rolled Back .......\n");
					sessionObj.getTransaction().rollback();
				}
				//sqlException.printStackTrace();
				return null;
			} finally {
				if(sessionObj != null) {
					sessionObj.close();
				}
			}
			
			return id;
		}
	
	// Method 2 RETRIVE: method used to display the records within the database tables
	public static List<Object> displayRecords(String entity_name) {
		List<Object> record_list = new ArrayList<Object>();		
		
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			record_list = sessionObj.createQuery("FROM " + entity_name).list();
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
			
		return record_list;
	}
	
	// Method 3 UPDATE: this method is used to update an existing record within the database tables	
	public static void updateRecord(Object entity_dao) {
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			//update the record which has the same id
			sessionObj.update(entity_dao);

			// Committing the transactions to the database
			sessionObj.getTransaction().commit();
			logger.info("\nSuccessfully updated the record in the database!\n");
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
	}
	
	// Method 4 DELETE: this method is used to delete a specific record within the database tables
	public static void deleteRecord(Object entity_dao) {
		try {
			// Getting Session Object From SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting Transaction Object From Session Object
			sessionObj.beginTransaction();
			
			//delete the record which has the same id
			sessionObj.delete(entity_dao);

			// Committing The Transactions To The Database
			sessionObj.getTransaction().commit();
			logger.info("\nRecord successfully deleted from the database!\n");
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n.......Transaction Is Being Rolled Back.......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
	}
	
	// Method 5 find: method used to find a specific record within the database tables
	public static Object findRecord(String entity_name, int id) {
		Object record_by_id = null;		
			
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			record_by_id = sessionObj.createQuery("FROM " + entity_name + " WHERE id = " + id).list().get(0);
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
				
		return record_by_id;
	}
	
	// Override Method 5 find: method used to find a specific record within the database tables
	public static Object findRecord(String entity_name, String username) {
		Object record_by_username = null;		
			
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			record_by_username = sessionObj.createQuery("FROM " + entity_name + " WHERE username = \'" + username + "\'").list().get(0);
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
				
		return record_by_username;
	}
	
	// Override Method 5 find: method used to find a specific record within the database tables
	public static List<Object> findRecord(String query) {
		List<Object> records = null;		
			
		try {
			// Getting sessionOject from SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting transaction object from SessionObject
			sessionObj.beginTransaction();

			records = sessionObj.createQuery(query).list();
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n....... Transaction Is Being Rolled Back .......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
				
		return records;
	}
	
	// Method 6 delete all: method used to delete all the row of a specific table
	public static void deleteAllRecord(String entity_name) {
		try {
			// Getting Session Object From SessionFactory
			sessionObj = buildSessionFactory().openSession();
			// Getting Transaction Object From Session Object
			sessionObj.beginTransaction();

			Query queryObj = sessionObj.createQuery("DELETE FROM " + entity_name);
			queryObj.executeUpdate();

			// Committing The Transactions To The Database
			sessionObj.getTransaction().commit();
			logger.info("\nSuccessfully deleted all the records from the table " + entity_name + "\n");
		} catch(Exception sqlException) {
			if(sessionObj.getTransaction() != null) {
				logger.error("\n.......Transaction Is Being Rolled Back.......\n");
				sessionObj.getTransaction().rollback();
			}
			//sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
	}
	
	// Method 7 emptyDB database: method used to empty the database's tables
	public static void emptyDB() throws ParseException {
			
		deleteAllRecord("Rating");
		deleteAllRecord("Movie");
		deleteAllRecord("Actor");
		deleteAllRecord("Director");
		deleteAllRecord("Genre");
		deleteAllRecord("Studio");
		deleteAllRecord("User");
			
	}
	
	// Method 8 populate database: method used to populate the database's tables
	public static void populateDB() throws ParseException {
		
		emptyDB();
		
		//Creation of some Actor Records
		ActorDao.createActor("Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 30);
		ActorDao.createActor("Daniel", "Radcliffe", new SimpleDateFormat("dd/MM/yyyy").parse("23/07/1989"), Gender.valueOf("MALE") , 10);
		ActorDao.createActor("Emma", "Watson", new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1990"), Gender.valueOf("FEMALE") , 12);
		ActorDao.createActor("Jennifer Shrader", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("15/08/1990"), Gender.valueOf("FEMALE") , 15);
		ActorDao.createActor("Sylvester", "Stallone", new SimpleDateFormat("dd/MM/yyyy").parse("06/07/1946"), Gender.valueOf("MALE") , 50);
		ActorDao.createActor("Joshua Ryan", "Hutcherson", new SimpleDateFormat("dd/MM/yyyy").parse("12/10/1992"), Gender.valueOf("MALE"), 27);
		ActorDao.createActor("Liam", "Hemsworth", new SimpleDateFormat("dd/MM/yyyy").parse("13/01/1990"), Gender.valueOf("MALE"), 15);
		ActorDao.createActor("Sam", "Worthington",  new SimpleDateFormat("dd/MM/yyyy").parse("02/08/1976"), Gender.valueOf("MALE"), 32);

		//Creation of some Director Records
		DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);
		DirectorDao.createDirector("Francis", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("26/03/1971"), Gender.valueOf("MALE") , 15);
		DirectorDao.createDirector("John Guilbert", "Avildsen", new SimpleDateFormat("dd/MM/yyyy").parse("21/12/1935"), Gender.valueOf("MALE") , 20);
		DirectorDao.createDirector("Chris", "Columbus", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 8);
		DirectorDao.createDirector("Alfonso", "Cuarón", new SimpleDateFormat("dd/MM/yyyy").parse("10/09/1958"), Gender.valueOf("MALE") , 25);
		DirectorDao.createDirector("Jon", "Favreau", new SimpleDateFormat("dd/MM/yyyy").parse("19/10/1966"), Gender.valueOf("MALE") , 9);
		DirectorDao.createDirector("Shane", "Black", new SimpleDateFormat("dd/MM/yyyy").parse("16/12/1961"), Gender.valueOf("MALE") , 13);
		DirectorDao.createDirector("James", "Cameron", new SimpleDateFormat("dd/MM/yyyy").parse("16/08/1954"), Gender.valueOf("MALE") , 10);

		// Creation of some Genre records
		GenreDao.createGenre("Horror", null);
		GenreDao.createGenre("Action", null);
		GenreDao.createGenre("Commedy", null);
		GenreDao.createGenre("Fantasy", null);
		GenreDao.createGenre("Thriller", null);
		GenreDao.createGenre("Adventure", null);
		GenreDao.createGenre("Science fiction", null);
		
		// Creation of some Studio records
		StudioDao.createStudio("Marvel Studios", "USA");
		StudioDao.createStudio("20th Century Fox", "USA");
		StudioDao.createStudio("Warner Bros.", "USA");
		StudioDao.createStudio("Paramount Pictures", "USA");
		StudioDao.createStudio("Carolco Pictures", "USA");
		StudioDao.createStudio("Universal Pictures", "USA");
		
		// Creation of some User records
		UserDao.createUser("coich", "p4ssw0rd", new Date());
		UserDao.createUser("vita", "password", new Date());
		UserDao.createUser("user3", "prova", new Date());
		UserDao.createUser("hackerbaldo", "hackerino", new Date());
		UserDao.createUser("galimone", "gymBione", new Date());
		
		// Creation of some Movie records
		MovieDao.createMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), null, 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Warner Bros.\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Ross\'").get(0)), null);
		MovieDao.createMovie("Hunger Games: la ragazza di fuoco", 146, new SimpleDateFormat("dd/MM/yyyy").parse("27/11/2013"), "Sequel of \"The Hunger Games\"", 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Adventure\'").get(0)),
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Universal Pictures\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Lawrence\'").get(0)),
				Arrays.asList(((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0))));
		MovieDao.createMovie("Hunger Games: Il canto della rivolta - Parte 1", 123, new SimpleDateFormat("dd/MM/yyyy").parse("20/11/2014"), "Sequel of \"Hunger Games: la ragazza di fuoco\"", 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Adventure\'").get(0)),
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Universal Pictures\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hemsworth\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Lawrence\'").get(0)),
				Arrays.asList(((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0)),
						((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Hunger Games: la ragazza di fuoco\'").get(0))));
		MovieDao.createMovie("Hunger Games: Il canto della rivolta - Parte 2", 137, new SimpleDateFormat("dd/MM/yyyy").parse("19/11/2015"), "Sequel of \"Hunger Games: Il canto della rivolta - Parte 1\"", 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Adventure\'").get(0)),
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Universal Pictures\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hemsworth\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Lawrence\'").get(0)),
				Arrays.asList(((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0)),
						((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Hunger Games: la ragazza di fuoco\'").get(0)),
						((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Hunger Games: Il canto della rivolta - Parte 1\'").get(0))));
		MovieDao.createMovie("Avatar", 161, new SimpleDateFormat("dd/MM/yyyy").parse("15/01/2010"), "Live the Na'vi!", 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'20th Century Fox\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Worthington\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Cameron\'").get(0)), null);
	
		// Creation of some Rating records
		RatingDao.createRating(((User) EntityService.findRecord("FROM User WHERE username = \'vita\'").get(0)),
				((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0)),
				3.8f, new Date(), "Super!");
		RatingDao.createRating(((User) EntityService.findRecord("FROM User WHERE username = \'coich\'").get(0)),
				((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0)),
				4.6f, new Date(), "Awesome!");
		RatingDao.createRating(((User) EntityService.findRecord("FROM User WHERE username = \'vita\'").get(0)),
				((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Avatar\'").get(0)),
				5.0f, new Date(), "The best movie ever!");
		RatingDao.createRating(((User) EntityService.findRecord("FROM User WHERE username = \'galimone\'").get(0)),
				((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Hunger Games: la ragazza di fuoco\'").get(0)),
				2.5f, new Date(), null);
		RatingDao.createRating(((User) EntityService.findRecord("FROM User WHERE username = \'hackerbaldo\'").get(0)),
				((Movie) EntityService.findRecord("FROM Movie WHERE title = \'Hunger Games: Il canto della rivolta - Parte 1\'").get(0)),
				3.0f, new Date(), "The first two were better");
	}
}
