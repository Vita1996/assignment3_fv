package assignment3.dao;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import assignment3.entities.Genre;
import assignment3.entities.Studio;
import assignment3.entities.Actor;
import assignment3.entities.Director;
import assignment3.entities.Movie;

public class MovieDao {
	
	public MovieDao() {}
	
	public static int createMovie(String title, int runtime, Date release_date, String overview, List<Genre> genre_list, Studio studio, List<Actor> actor_list, Director director, List<Movie> sequel_list) {
		
		Movie movie = new Movie();
		movie.setTitle(title);
		movie.setRuntime(runtime);
		movie.setReleaseDate(release_date);
		movie.setOverview(overview);
		
		if(genre_list != null) {
			for (ListIterator<Genre> iter = genre_list.listIterator(); iter.hasNext(); ) {
			    Genre element = iter.next();
			    movie.setGenres(element);
			}
		}
		else
			movie.setGenres(null);
		
		if(studio != null) 
			movie.setStudio(studio);
		else
			movie.setStudio(null);
		
		if(actor_list != null)  {
			for (ListIterator<Actor> iter = actor_list.listIterator(); iter.hasNext(); ) {
			    Actor element = iter.next();
			    movie.setActors(element);
			}
		}
		else
			movie.setActors(null);
		
		if(director != null) 
			movie.setDirector(director);
		else
			movie.setDirector(null);
		
		if(sequel_list != null) {
			for (ListIterator<Movie> iter = sequel_list.listIterator(); iter.hasNext(); ) {
			    Movie element = iter.next();
			    movie.setSequels(element);
			}
		}
		else
			movie.setSequels(null);
		
		return EntityService.createRecord(movie);
	}
	
	public static void retriveMovie() {
		
		List<Object> movie_records = EntityService.displayRecords("Movie");
		
		if(movie_records != null && movie_records.size()>0) {
			
			System.out.println("\nThere are " + movie_records.size() + " records in the Movie table:");
			for(Object record : movie_records)
				System.out.println(((Movie) record).toString());
		}
		else
			System.out.println("\nThe Movie table is still empty!");
	}
	
	public static void updateMovie(int id, String new_title, int new_runtime, Date new_release_date, String new_overview, Collection<Genre> new_genre_list, Studio new_studio, Collection<Actor> new_actor_list, Director new_director, Collection<Movie> new_sequel_list) {
		
		Movie update_movie = new Movie();
		update_movie.setId(id);
		update_movie.setTitle(new_title);
		update_movie.setRuntime(new_runtime);
		update_movie.setReleaseDate(new_release_date);
		update_movie.setOverview(new_overview);
		if(new_genre_list != null)	
			for (Iterator<Genre> iter = new_genre_list.iterator(); iter.hasNext(); ) {
			    Genre element = iter.next();
			    update_movie.setGenres(element);
			}
		else
			update_movie.setGenres(null);
		if(new_studio != null)
			update_movie.setStudio(new_studio);
		else
			update_movie.setStudio(null);
		if(new_actor_list != null)	
			for (Iterator<Actor> iter = new_actor_list.iterator(); iter.hasNext(); ) {
			    Actor element = iter.next();
			    update_movie.setActors(element);
			}
		else
			update_movie.setActors(null);
		if(new_director != null)
			update_movie.setDirector(new_director);
		else
			update_movie.setDirector(null);
		if(new_sequel_list != null)
			for (Iterator<Movie> iter = new_sequel_list.iterator(); iter.hasNext(); ) {
				Movie element = iter.next();
				update_movie.setSequels(element);
			}
		else
			update_movie.setSequels(null);
		
		EntityService.updateRecord(update_movie);
	}
	
	public static void deleteMovie(int id) {
		
		Movie deleted_movie = new Movie();
		
		deleted_movie.setId(id);
		
		EntityService.deleteRecord(deleted_movie);
	}
	
	public static void deleteAllMovies() {
		
		EntityService.deleteAllRecord("Movie");
	}
	
	public static Movie findMovie(int id) {
		
		Movie get_movie = new Movie();
		
		get_movie = (Movie) EntityService.findRecord("Movie", id);
		if(get_movie != null)
			System.out.println(get_movie.toString());
		
		return get_movie;
	}
}
