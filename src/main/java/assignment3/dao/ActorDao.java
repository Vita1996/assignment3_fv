package assignment3.dao;

import java.util.Date;
import java.util.List;

import assignment3.entities.Actor;
import assignment3.entities.Person.Gender;

public class ActorDao {
	
	public ActorDao() {}
	
	public static int createActor(String firstname, String lastname, Date birthdate, Gender gender, int film_acted) {
		
		Actor actor = new Actor();
		actor.setFirstname(firstname);
		actor.setLastname(lastname);
		actor.setBirthdate(birthdate);
		actor.setGender(gender);
		actor.setFilm_acted(film_acted);

		return EntityService.createRecord(actor);
	}
	
	public static void retriveActor() {
		
		List<Object> actor_records = EntityService.displayRecords("Actor");
		
		if(actor_records != null && actor_records.size()>0) {
			
			System.out.println("\nThere are " + actor_records.size() + " records in the Actor table:");
			for(Object record : actor_records)
				System.out.println(((Actor) record).toString());
		}
		else
			System.out.println("\nThe Actor table is still empty!");
	}
	
	public static void updateActor(int id, String new_firstname, String new_lastname, Date new_birthdate, Gender new_gender, int new_film_acted) {
		
		Actor update_actor = new Actor();
		
		update_actor.setId(id);
		update_actor.setFirstname(new_firstname);
		update_actor.setLastname(new_lastname);
		update_actor.setBirthdate(new_birthdate);
		update_actor.setGender(new_gender);
		update_actor.setFilm_acted(new_film_acted);
			
		EntityService.updateRecord(update_actor);
	}
	
	public static void deleteActor(int id) {
		
		Actor deleted_actor = new Actor();
		
		deleted_actor.setId(id);
		
		EntityService.deleteRecord(deleted_actor);
	}
	
	public static void deleteAllActors() {
		
		EntityService.deleteAllRecord("Actor");
	}
	
	public static Actor findActor(int id) {
		
		Actor get_actor = new Actor();
		
		get_actor = (Actor) EntityService.findRecord("Actor", id);
		if(get_actor != null)
			System.out.println(get_actor.toString());
		
		return get_actor;
	}
}
