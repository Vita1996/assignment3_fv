package assignment3.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import assignment3.dao.RatingDao;
import assignment3.dao.EntityService;
import assignment3.entities.Movie;
import assignment3.entities.Rating;
import assignment3.entities.User;

public class RatingInterface {
private static Scanner reader = new Scanner(System.in);
	
	public RatingInterface() {}
	
	public static void createRating() throws ParseException {
		
		User rating_user = null;
		Movie rating_movie = null;
		float rating_vote;
		String rating_comment;
		Date rating_date_vote;
		
		List<Object> records_list;
		List<Object> records = null;
		String rating_fk;
		
		// User
		records_list = EntityService.displayRecords("User");
		System.out.print("\nEnter the user's username of the rating ( ");
		if(records_list == null || records_list.size()<=0)
			System.out.print("no user available ): ");
		else
		{
			for(Object user_username : records_list)
				System.out.print(((User) user_username).getUsername() + ", ");
			System.out.println("): ");
		}
		rating_fk = reader.nextLine();
			
		boolean exist = false;
		for(Object user_username : records_list)
			if(((User) user_username).getUsername().equalsIgnoreCase(rating_fk))
				exist = true;
			
		if(exist) 
		{
			records = EntityService.findRecord("FROM User WHERE username = \'" + rating_fk + "\'");
			System.out.println(records.get(0).toString());
			rating_user = (User) records.get(0);
		}
		else
			System.out.println("The user " + rating_fk + " does not exist! Create it!");

		// Movie
		records_list = EntityService.displayRecords("Movie");
		System.out.print("\nEnter the movie's title of the rating ( ");
		if(records_list == null || records_list.size()<=0)
			System.out.print("no user available ): ");
		else
		{
			for(Object movie_id : records_list)
				System.out.print(((Movie) movie_id).getTitle() + ", ");
			System.out.println("): ");
		}
		rating_fk = reader.nextLine();
					
		exist = false;
		for(Object movie_id : records_list)
			if(((Movie) movie_id).getTitle().equalsIgnoreCase(rating_fk))
				exist = true;
					
		if(exist) 
		{
			records = EntityService.findRecord("FROM Movie WHERE title = \'" + rating_fk + "\'");
			System.out.println(records.get(0).toString());
			rating_movie = (Movie) records.get(0);
		}
		else
			System.out.println("The movie " + rating_fk + " does not exist! Create it!");
		
		System.out.println("Enter the vote of the movie: ");
		rating_vote = reader.nextFloat();
		reader.nextLine();
		if(rating_vote < 0 || rating_vote > 10)
			rating_vote = -1;
		
		System.out.println("Enter the vote date of the rating (dd/MM/yyyy): ");
		String read_date = reader.nextLine();
		if(read_date.equals("")) 
			rating_date_vote = new Date();
		else
			rating_date_vote = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);  
				
		System.out.println("Enter the comment of the rating (empty is possible): ");
		rating_comment = reader.nextLine();
		if(rating_comment.equals(""))
			rating_comment = null;
		
		if(rating_user != null && rating_movie != null && rating_vote != -1)
			RatingDao.createRating(rating_user, rating_movie, rating_vote, rating_date_vote, rating_comment);
		else
			System.out.println("\nThe rating's user or movie or vote can't be null!\nRating record not created!");

	}
	
	public static void updateRating() throws ParseException {
		
		Rating update_rating = new Rating();
		User new_user = null;
		Movie new_movie = null;
		int rating_id;
		float new_rating_vote = -1;
		Date new_rating_date_vote = null;
		String new_rating_comment = null;
		
		List<Object> new_records_list;
		List<Object> new_records = null;
		String new_rating_fk;
		
		System.out.println("Enter the ID of the rating record you want to update: ");
		rating_id = reader.nextInt();
		reader.nextLine();	// burn the new line of submission
		update_rating = (Rating) EntityService.findRecord("Rating", rating_id);
		
		if(update_rating != null) {
			
			// Update User
			System.out.println("Enter the new user of the rating (previus is " + update_rating.getUser().getUsername() +" / n for no change): ");
			new_rating_fk = reader.nextLine();
			if(!new_rating_fk.equals("n")) {
				new_records_list = EntityService.displayRecords("User");

				boolean exist = false;
				for(Object user_username : new_records_list)
					if(((User) user_username).getUsername().equalsIgnoreCase(new_rating_fk))
						exist = true;
				
				if(exist) 
				{
					new_records = EntityService.findRecord("FROM User WHERE username = \'" + new_rating_fk + "\'");
					System.out.println(new_records.get(0).toString());
					new_user = (User) new_records.get(0);
				}
				else
					System.out.println("The user " + new_rating_fk + " does not exist! Create it!");
			}
			
			// Update Movie
			System.out.println("Enter the new movie of the rating (previus is " + update_rating.getMovie().getTitle() +" / n for no change): ");
			new_rating_fk = reader.nextLine();
			if(!new_rating_fk.equals("n")) {
				new_records_list = EntityService.displayRecords("Movie");

				boolean exist = false;
				for(Object movie_id : new_records_list)
					if(((Movie) movie_id).getTitle().equalsIgnoreCase(new_rating_fk))
						exist = true;
							
				if(exist) 
				{
					new_records = EntityService.findRecord("FROM Movie WHERE title = \'" + new_rating_fk + "\'");
					System.out.println(new_records.get(0).toString());
					new_movie = (Movie) new_records.get(0);
				}
				else
					System.out.println("The movie " + new_rating_fk + " does not exist! Create it!");
			}
			
			System.out.println("Enter the new vote of the rating (previus is " + update_rating.getVote() +" / -1 for no change):");
			new_rating_vote = reader.nextFloat();
			reader.nextLine();
			if(new_rating_vote != -1)
				new_rating_vote = update_rating.getVote();
			
			System.out.println("Enter the new date of the rating (previus is " + update_rating.getDateVote() +" / n for no change): ");
			String read_date = reader.nextLine();			
			if(read_date.equals("n"))
				new_rating_date_vote = update_rating.getDateVote();
			else if(read_date.equals(""))
				new_rating_date_vote = new Date();
			else
				new_rating_date_vote = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
			
			System.out.println("Enter the new comment of the rating (previus is " + update_rating.getComment() +" / n for no change): ");
			new_rating_comment = reader.nextLine();
			if(new_rating_comment.equals("n"))
				new_rating_comment = update_rating.getComment();
			else if(new_rating_comment.equals(""))
				new_rating_comment = null;
		}		
		
		if(new_user != null && new_movie != null && new_rating_vote != -1)
			RatingDao.updateRating(rating_id, new_user, new_movie, new_rating_vote, new_rating_date_vote, new_rating_comment);
		else
			System.out.println("\nThe rating's user or movie or vote can't be null!\nRating record not updated!");
	}
	
	public static void deleteRating() {
		
		int rating_id;
		
		System.out.println("Enter the ID of the rating record you want to delete: ");
		rating_id = reader.nextInt();
		
		RatingDao.deleteRating(rating_id);
	}
	
	public static void findRating() {
		
		int rating_id;
		
		System.out.println("Enter the ID of the rating record you want to find: ");
		rating_id = reader.nextInt();
		
		RatingDao.findRating(rating_id);
	}
}
