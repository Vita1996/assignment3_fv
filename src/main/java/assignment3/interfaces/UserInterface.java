package assignment3.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import assignment3.dao.EntityService;
import assignment3.dao.UserDao;
import assignment3.entities.User;

public class UserInterface {
	
	private static Scanner reader = new Scanner(System.in);
	
	public UserInterface() {}
	
	public static void createUser() throws ParseException {
		
		String user_username, user_password;
		Date user_registration_date;
		
		System.out.println("Enter the username of the user: ");
		user_username = reader.nextLine();
		if(user_username.equals(""))
			user_username = null;
		
		System.out.println("Enter the password of the user: ");
		user_password = reader.nextLine();
		if(user_password.equals(""))
			user_password = null;
		
		System.out.println("Enter the registration's date of the user(format dd/MM/yyyy): ");
		String read_date = reader.nextLine();
		if(read_date.equals("")) 
			user_registration_date = new Date();
		else
			user_registration_date = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
		
		if(user_username != null && user_password != null)
			UserDao.createUser(user_username, user_password, user_registration_date);
		else
			System.out.println("\nThe user's username or password can't be null!\nUser record not created!");
	}
	
	public static void updateUser() throws ParseException {
		
		User update_user = new User();
		String user_username, new_user_password = null;
		Date new_user_registration_date = null;
		
		System.out.println("Enter the user of the username record you want to update: ");
		user_username = reader.nextLine();
		update_user = (User) EntityService.findRecord("User", user_username);
		
		if(update_user != null) {
			
			System.out.println("Enter the new password of the user(previus is " + update_user.getPassword() +" / n for no change): ");
			new_user_password = reader.nextLine();
			if(new_user_password.equals("n"))
				new_user_password = update_user.getPassword();
			else if(new_user_password.equals(""))
				new_user_password = null;
			
			System.out.println("Enter the new description of the genre(previus is " + update_user.getRegistrationDate() +" / n for no change): ");
			String read_date = reader.nextLine();
			
			if(read_date.equals("n"))
				new_user_registration_date = update_user.getRegistrationDate();
			else if(read_date.equals(""))
				new_user_registration_date = new Date();
			else
				new_user_registration_date = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
		}
		
		if(user_username != null && new_user_password != null)
			UserDao.updateUser(user_username, new_user_password, new_user_registration_date);
		else if(update_user != null)
			System.out.println("\nThe user's username or password can't be null!\nUser record not updated!");
	}
	
	public static void deleteUser() {
		
		String user_username;
		
		System.out.println("Enter the username of the user record you want to delete: ");
		user_username = reader.nextLine();
		
		UserDao.deleteUser(user_username);
	}
	
	public static void findUser() {
		
		String user_username;
		
		System.out.println("Enter the username of the user record you want to find: ");
		user_username = reader.nextLine();
		
		UserDao.findUser(user_username);
	}
}
