package assignment3.interfaces;

import java.util.Scanner;

import assignment3.dao.EntityService;
import assignment3.dao.StudioDao;
import assignment3.entities.Studio;

public class StudioInterface {
	private static Scanner reader = new Scanner(System.in);

	public StudioInterface() {}
	
	public static void createStudio() {
		
		String studio_name, studio_nation;
		
		System.out.println("Enter the name of the studio: ");
		studio_name = reader.nextLine();
		if(studio_name.equals(""))
			studio_name = null;
		
		System.out.println("Enter the nation of the studio: ");
		studio_nation = reader.nextLine();
		if(studio_nation.equals(""))
			studio_nation = null;
		
		if(studio_name != null)
			StudioDao.createStudio(studio_name, studio_nation);
		else
			System.out.println("\nThe studio's name can't be null!\nStudio record not created!");
	}
	
	public static void updateStudio() {
		
		Studio update_studio = new Studio();
		int studio_id;
		String new_studio_name = null, new_studio_nation = null;
		
		System.out.println("Enter the ID of the studio record you want to update: ");
		studio_id = reader.nextInt();
		reader.nextLine();
		update_studio = (Studio) EntityService.findRecord("Studio", studio_id);
		
		if(update_studio != null) {
			
			System.out.println("Enter the new name of the studio(previus is " + update_studio.getName() +" / n for no change): ");
			new_studio_name = reader.nextLine();
			if(new_studio_name.equals("n"))
				new_studio_name = update_studio.getName();
			else if(new_studio_name.equals(""))
				new_studio_name = null;
			
			System.out.println("Enter the new nation of the studio(previus is " + update_studio.getNation() +" / n for no change): ");
			new_studio_nation = reader.nextLine();
			
			if(new_studio_nation.equals("n"))
				new_studio_nation = update_studio.getNation();
			else if(new_studio_nation.equals(""))
				new_studio_nation = null;
		}
		
		if(new_studio_name != null)
			StudioDao.updateStudio(studio_id, new_studio_name, new_studio_nation);
		else if(update_studio != null)
			System.out.println("\nThe studio's name can't be null!\nStudio record not updated!");
	}
	
	public static void deleteStudio() {
		
		int studio_id;
		
		System.out.println("Enter the ID of the studio record you want to delete: ");
		studio_id = reader.nextInt();
		
		StudioDao.deleteStudio(studio_id);
	}
	
	public static void findStudio() {
		
		int studio_id;
		
		System.out.println("Enter the ID of the studio record you want to find: ");
		studio_id = reader.nextInt();
		
		StudioDao.findStudio(studio_id);
	}
}
