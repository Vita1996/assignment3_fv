package assignment3.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import assignment3.dao.DirectorDao;
import assignment3.dao.EntityService;
import assignment3.entities.Director;
import assignment3.entities.Person.Gender;

public class DirectorInterface {
private static Scanner reader = new Scanner(System.in);
	
	public DirectorInterface() {}
	
	public static void createDirector() throws ParseException {
		
		String director_firstname, director_lastname;
		Date director_birthdate;
		Gender director_gender = null;
		int film_directed;
		
		System.out.println("Enter the firstname of the director: ");
		director_firstname = reader.nextLine();
		if(director_firstname.equals(""))
			director_firstname = null;
		
		System.out.println("Enter the lastname of the director: ");
		director_lastname = reader.nextLine();
		if(director_lastname.equals(""))
			director_lastname = null;
		
		System.out.println("Enter the birthdate of the director (dd/MM/yyyy): ");
		String read_date = reader.nextLine();
		if(read_date.equals("")) 
			director_birthdate = null;
		else
			director_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(read_date); 
		
		System.out.println("Enter the gender of the director " + java.util.Arrays.asList(Gender.values()) + ": ");
		try {
			director_gender = Gender.valueOf(reader.nextLine().toUpperCase());
		} catch (IllegalArgumentException m) {
			System.out.println("\nInvalid input!\n");
			director_gender = null;
		}
		
		System.out.println("Enter the number of film acted by the director: ");
		film_directed = reader.nextInt();
				
		if(director_firstname != null && director_lastname != null)
			DirectorDao.createDirector(director_firstname, director_lastname, director_birthdate, director_gender, film_directed);
		else
			System.out.println("\nThe director's firstname or lastname can't be null!\nDirector record not created!");
	}
	
	public static void updateDirector() throws ParseException {
		
		Director update_director = new Director();
		int director_id;
		String new_director_firstname = null, new_director_lastname = null;
		Date new_director_birthdate = null;
		Gender new_director_gender = null;
		int new_film_directed = -1;
		
		System.out.println("Enter the ID of the director record you want to update: ");
		director_id = reader.nextInt();
		reader.nextLine();	// burn the new line of submission
		update_director = (Director) EntityService.findRecord("Director", director_id);
		
		if(update_director != null) {
			
			System.out.println("Enter the new firstname of the director (previus is " + update_director.getFirstname() + " / n for no change): "); 
			new_director_firstname = reader.nextLine();
			if(new_director_firstname.equals("n"))
				new_director_firstname = update_director.getFirstname();
			else if(new_director_firstname.equals(""))
				new_director_firstname = null;
						
			System.out.println("Enter the new lastname of the director (previus is " + update_director.getLastname() + " / n for no change): "); 
			new_director_lastname = reader.nextLine();
			if(new_director_lastname.equals("n"))
				new_director_lastname = update_director.getFirstname();
			else if(new_director_lastname.equals(""))
				new_director_lastname = null;
			
			System.out.println("Enter the new birthdate of the director (previus is " + update_director.getBirthdate() +" / n for no change): ");
			String read_date = reader.nextLine();
			if(read_date.equals("n"))
				new_director_birthdate = update_director.getBirthdate();
			else if(read_date.equals(""))
				new_director_birthdate = null;
			else
				new_director_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
			
			System.out.println("Enter the new gender of the director " + java.util.Arrays.asList(Gender.values()) + " (previus is " + update_director.getGender() +" / n for no change): ");
			String read_gender = reader.nextLine().toUpperCase();
			if(!read_gender.equals("N")) {
				try {
					new_director_gender = Gender.valueOf(read_gender);
				} catch (IllegalArgumentException m) {
					System.out.println("\nInvalid input!\n");
					new_director_gender = null;
				}
			}
			else
				new_director_gender = update_director.getGender();

			System.out.println("Enter the new number of film acted by the director (previus is " + update_director.getFilm_directed() + " / -1 for no change): "); 
			new_film_directed = reader.nextInt();
			if(new_film_directed != -1)
				new_film_directed = update_director.getFilm_directed();
		}
		
		if(new_director_firstname != null && new_director_lastname != null)
			DirectorDao.updateDirector(director_id, new_director_firstname, new_director_lastname, new_director_birthdate, new_director_gender, new_film_directed);
		else
			System.out.println("\nThe director's firstname or lastname can't be null!\nDirector record not updated!");
	}
	
	public static void deleteDirector() {
		
		int director_id;
		
		System.out.println("Enter the ID of the director record you want to delete: ");
		director_id = reader.nextInt();
		
		DirectorDao.deleteDirector(director_id);
	}
	
	public static void findDirector() {
		
		int director_id;
		
		System.out.println("Enter the ID of the director record you want to find: ");
		director_id = reader.nextInt();
		
		DirectorDao.findDirector(director_id);
	}
}