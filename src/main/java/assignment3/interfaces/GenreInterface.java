package assignment3.interfaces;

import java.util.Scanner;

import assignment3.dao.EntityService;
import assignment3.dao.GenreDao;
import assignment3.entities.Genre;

public class GenreInterface {
	private static Scanner reader = new Scanner(System.in);
	
	public GenreInterface() {}
	
	public static void createGenre() {
		
		String genre_name, genre_description;
		
		System.out.println("Enter the name of the genre: ");
		genre_name = reader.nextLine();
		if(genre_name.equals(""))
			genre_name = null;
		
		System.out.println("Enter the description of the genre(empty is possible): ");
		genre_description = reader.nextLine();
		if(genre_description.equals(""))
			genre_description = null;
		
		if(genre_name != null)
			GenreDao.createGenre(genre_name, genre_description);
		else
			System.out.println("\nThe genre's name can't be null!\\nGenre record not created!");
	}
	
	public static void updateGenre() {
		
		Genre update_genre = new Genre();
		int genre_id;
		String new_genre_name = null, new_genre_description = null;
		
		System.out.println("Enter the ID of the genre record you want to update: ");
		genre_id = reader.nextInt();
		reader.nextLine();
		update_genre = (Genre) EntityService.findRecord("Genre", genre_id);
		
		if(update_genre != null) {
			
			System.out.println("Enter the new name of the genre(previus is " + update_genre.getName() +" / n for no change): ");
			new_genre_name = reader.nextLine();
			if(new_genre_name.equals("n"))
				new_genre_name = update_genre.getName();
			else if(new_genre_name.equals(""))
				new_genre_name = null;
			
			System.out.println("Enter the new description of the genre(previus is " + update_genre.getDescription() +" / n for no change): ");
			new_genre_description = reader.nextLine();
			
			if(new_genre_description.equals("n"))
				new_genre_description = update_genre.getDescription();
			else if(new_genre_description.equals(""))
				new_genre_description = null;
		}
		
		if(new_genre_name != null)
			GenreDao.updateGenre(genre_id, new_genre_name, new_genre_description);
		else if(update_genre != null)
			System.out.println("\nThe genre's name can't be null!\nGenre record not updated!");
	}
	
	public static void deleteGenre() {
		
		int genre_id;
		
		System.out.println("Enter the ID of the genre record you want to delete: ");
		genre_id = reader.nextInt();
		
		GenreDao.deleteGenre(genre_id);
	}
	
	public static void findGenre() {
		
		int genre_id;
		
		System.out.println("Enter the ID of the genre record you want to find: ");
		genre_id = reader.nextInt();
		
		GenreDao.findGenre(genre_id);
	}
}
