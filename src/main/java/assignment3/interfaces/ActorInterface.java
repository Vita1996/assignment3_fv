package assignment3.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import assignment3.dao.EntityService;
import assignment3.dao.ActorDao;
import assignment3.entities.Actor;
import assignment3.entities.Person.Gender;

public class ActorInterface {
private static Scanner reader = new Scanner(System.in);
	
	public ActorInterface() {}
	
	public static void createActor() throws ParseException {
		
		String actor_firstname, actor_lastname;
		Date actor_birthdate;
		Gender actor_gender = null;
		int film_acted;
		
		System.out.println("Enter the firstname of the actor: ");
		actor_firstname = reader.nextLine();
		if(actor_firstname.equals(""))
			actor_firstname = null;
		
		System.out.println("Enter the lastname of the actor: ");
		actor_lastname = reader.nextLine();
		if(actor_lastname.equals(""))
			actor_lastname = null;
		
		System.out.println("Enter the birthdate of the actor (dd/MM/yyyy): ");
		String read_date = reader.nextLine();
		if(read_date.equals("")) 
			actor_birthdate = null;
		else
			actor_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(read_date); 
		
		System.out.println("Enter the gender of the actor " + java.util.Arrays.asList(Gender.values()) + ": ");
		try {
			actor_gender = Gender.valueOf(reader.nextLine().toUpperCase());
		} catch (IllegalArgumentException m) {
			System.out.println("\nInvalid input!\n");
			actor_gender = null;
		}
		
		System.out.println("Enter the number of film acted by the actor: ");
		film_acted = reader.nextInt();
				
		if(actor_firstname != null && actor_lastname != null)
			ActorDao.createActor(actor_firstname, actor_lastname, actor_birthdate, actor_gender, film_acted);
		else
			System.out.println("\nThe actor's firstname or lastname can't be null!\nActor record not created!");
	}
	
	public static void updateActor() throws ParseException {
		
		Actor update_actor = new Actor();
		int actor_id;
		String new_actor_firstname = null, new_actor_lastname = null;
		Date new_actor_birthdate = null;
		Gender new_actor_gender = null;
		int new_film_acted = -1;
		
		System.out.println("Enter the ID of the actor record you want to update: ");
		actor_id = reader.nextInt();
		reader.nextLine();	// burn the new line of submission
		update_actor = (Actor) EntityService.findRecord("Actor", actor_id);
		
		if(update_actor != null) {
			
			System.out.println("Enter the new firstname of the actor (previus is " + update_actor.getFirstname() + " / n for no change): "); 
			new_actor_firstname = reader.nextLine();
			if(new_actor_firstname.equals("n"))
				new_actor_firstname = update_actor.getFirstname();
			else if(new_actor_firstname.equals(""))
				new_actor_firstname = null;
						
			System.out.println("Enter the new lastname of the actor (previus is " + update_actor.getLastname() + " / n for no change): "); 
			new_actor_lastname = reader.nextLine();
			if(new_actor_lastname.equals("n"))
				new_actor_lastname = update_actor.getFirstname();
			else if(new_actor_lastname.equals(""))
				new_actor_lastname = null;
			
			System.out.println("Enter the new birthdate of the actor (previus is " + update_actor.getBirthdate() +" / n for no change): ");
			String read_date = reader.nextLine();
			if(read_date.equals("n"))
				new_actor_birthdate = update_actor.getBirthdate();
			else if(read_date.equals(""))
				new_actor_birthdate = null;
			else
				new_actor_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
			
			System.out.println("Enter the new gender of the actor " + java.util.Arrays.asList(Gender.values()) + " (previus is " + update_actor.getGender() +" / n for no change): ");
			String read_gender = reader.nextLine().toUpperCase();
			if(!read_gender.equals("N")) {
				try {
					new_actor_gender = Gender.valueOf(read_gender);
				} catch (IllegalArgumentException m) {
					System.out.println("\nInvalid input!\n");
					new_actor_gender = null;
				}
			}
			else
				new_actor_gender = update_actor.getGender();

			System.out.println("Enter the new number of film acted by the actor (previus is " + update_actor.getFilm_acted() + " / -1 for no change): "); 
			new_film_acted = reader.nextInt();
			if(new_film_acted != -1)
				new_film_acted = update_actor.getFilm_acted();
		}
		
		if(new_actor_firstname != null && new_actor_lastname != null)
			ActorDao.updateActor(actor_id, new_actor_firstname, new_actor_lastname, new_actor_birthdate, new_actor_gender, new_film_acted);
		else
			System.out.println("\nThe actor's firstname or lastname can't be null!\nActor record not updated!");
	}
	
	public static void deleteActor() {
		
		int actor_id;
		
		System.out.println("Enter the ID of the actor record you want to delete: ");
		actor_id = reader.nextInt();
		
		ActorDao.deleteActor(actor_id);
	}
	
	public static void findActor() {
		
		int actor_id;
		
		System.out.println("Enter the ID of the actor record you want to find: ");
		actor_id = reader.nextInt();
		
		ActorDao.findActor(actor_id);
	}
}
