package assignment3.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import assignment3.dao.EntityService;
import assignment3.dao.MovieDao;
import assignment3.entities.Actor;
import assignment3.entities.Director;
import assignment3.entities.Genre;
import assignment3.entities.Movie;
import assignment3.entities.Studio;

public class MovieInterface {
private static Scanner reader = new Scanner(System.in);
	
	public MovieInterface() {}
	
	public static void createMovie() throws ParseException {
		
		String movie_title, movie_overview;
		int movie_runtime;
		Date movie_release_date;
		List<Genre> genre_list = new ArrayList<Genre>();
		Studio studio = null;
		List<Actor> actor_list = new ArrayList<Actor>();
		Director director = null;
		List<Movie> sequel_list = new ArrayList<Movie>();
		
		List<Object> records_list;
		List<Object> records = null;
		String movie_fk, read;
		
		System.out.println("Enter the title of the movie: ");
		movie_title = reader.nextLine();
		if(movie_title.equals(""))
			movie_title = null;
		
		System.out.println("Enter the runtime of the movie: ");
		movie_runtime = reader.nextInt();
		reader.nextLine();
		
		System.out.println("Enter the release date of the movie (dd/MM/yyyy): ");
		String read_date = reader.nextLine();
		if(read_date.equals("")) 
			movie_release_date = null;
		else
			movie_release_date = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);  
				
		System.out.println("Enter an optional overwiew of the movie (empty is possible): ");
		movie_overview = reader.nextLine();
		if(movie_overview.equals(""))
			movie_overview = null;

		// Genre
		records_list = EntityService.displayRecords("Genre");
		do{
			System.out.print("\nEnter a genre's name of the movie ( ");
			if(records_list == null || records_list.size()<=0)
				System.out.print("no genres available ): ");
			else
			{
				for(Object genre_name : records_list)
					System.out.print(((Genre) genre_name).getName() + ", ");
				System.out.println("): ");
			}
			movie_fk = reader.nextLine();
			
			boolean exist = false;
			for(Object genre_name : records_list)
				if(((Genre) genre_name).getName().equalsIgnoreCase(movie_fk))
					exist = true;
			
			if(exist) 
			{
				records = EntityService.findRecord("FROM Genre WHERE name = \'" + movie_fk + "\'");
				System.out.println(records.get(0).toString());
				genre_list.add((Genre) records.get(0));
			}
			else
				System.out.println("The genre " + movie_fk + " does not exist! Create it!");
			
			System.out.println("\nDoes the movie have another genre? (y/n): ");
			read = reader.nextLine();
		} while(read.equals("y"));

		// Studio
		records_list = EntityService.displayRecords("Studio");
		System.out.print("\nEnter the studio's name of the movie ( ");
		if(records_list == null || records_list.size()<=0)
			System.out.print("no studios available ): ");
		else
		{
			for(Object studio_name : records_list)
				System.out.print(((Studio) studio_name).getName() + ", ");
			System.out.println("): ");
		}
		movie_fk = reader.nextLine();
			
		boolean studio_exist = false;
		for(Object studio_name : records_list)
			if(((Studio) studio_name).getName().equalsIgnoreCase(movie_fk))
				studio_exist = true;
			
		if(studio_exist) 
		{
			records = EntityService.findRecord("FROM Studio WHERE name = \'" + movie_fk + "\'");
			System.out.println(records.get(0).toString());
			studio = (Studio) records.get(0);
		}
		else
			System.out.println("The studio " + movie_fk + " does not exist! Create it!");
		
		// Actor
		records_list = EntityService.displayRecords("Actor");
		do{
			System.out.print("\nEnter an actor's lastname who acted in the movie ( ");
			if(records_list == null || records_list.size()<=0)
				System.out.print("no actors available ): ");
			else
			{
				for(Object actor_name : records_list)
					System.out.print(((Actor) actor_name).getFirstname() + " " + ((Actor) actor_name).getLastname() + ", ");
				System.out.println("): ");
			}
			movie_fk = reader.nextLine();
			
			boolean exist = false;
			for(Object actor_name : records_list)
				if(((Actor) actor_name).getLastname().equalsIgnoreCase(movie_fk))
					exist = true;
			
			if(exist) 
			{
				records = EntityService.findRecord("FROM Actor WHERE lastname = \'" + movie_fk + "\'");
				System.out.println(records.get(0).toString());
				actor_list.add((Actor) records.get(0));
			}
			else
				System.out.println("The actor " + movie_fk + " does not exist! Create it!");
			
			System.out.println("\nDoes the movie have another actor? (y/n): ");
			read = reader.nextLine();
		} while(read.equals("y"));
		
		// Director
		records_list = EntityService.displayRecords("Director");
		System.out.print("\nEnter the director's lastname of the movie ( ");
		if(records_list == null || records_list.size()<=0)
			System.out.print("no directors available ): ");
		else
		{
			for(Object director_name : records_list)
				System.out.print(((Director) director_name).fullName() + ", ");
			System.out.println("): ");
		}
		movie_fk = reader.nextLine();
			
		boolean director_exist = false;
		for(Object director_name : records_list)
			if(((Director) director_name).getLastname().equalsIgnoreCase(movie_fk))
				director_exist = true;
			
		if(director_exist) 
		{
			records = EntityService.findRecord("FROM Director WHERE lastname = \'" + movie_fk + "\'");
			director = (Director) records.get(0);
		}
		else
			System.out.println("The director " + movie_fk + " does not exist! Create it!");

		// Self-relation
		records_list = EntityService.displayRecords("Movie");
		do{
			System.out.print("\nEnter a movie title's that has relation with " + movie_title + " ( ");
			if(records_list == null || records_list.size()<=0)
				System.out.print("no movies available ): ");
			else
			{
				for(Object movie_name : records_list)
					System.out.print(((Movie) movie_name).getTitle() + ", ");
				System.out.print("): ");
			}
			movie_fk = reader.nextLine();
			
			boolean exist = false;
			for(Object movie_name : records_list)
				if(((Movie) movie_name).getTitle().equalsIgnoreCase(movie_fk))
					exist = true;
			
			if(exist) 
			{
				records = EntityService.findRecord("FROM Movie WHERE title = \'" + movie_fk + "\'");
				System.out.println(records.get(0).toString());
				sequel_list.add((Movie) records.get(0));
			}
			else
				System.out.println("The movie " + movie_fk + " does not exist! Create it!");
			
			System.out.println("\nDoes the movie have other relation? (y/n): ");
			read = reader.nextLine();
		} while(read.equals("y"));
		
		if(movie_title != null)
			MovieDao.createMovie(movie_title, movie_runtime, movie_release_date, movie_overview, genre_list, studio, actor_list, director, sequel_list);
		else
			System.out.println("\nThe movie's title can't be null!\nMovie record not created!");

	}
	
	public static void updateMovie() throws ParseException {
		
		Movie update_movie = new Movie();
		int movie_id;
		String new_movie_title = null, new_movie_overview = null;
		int new_movie_runtime = -1;
		Date new_movie_release_date = null;
		List<Genre> new_genre_list = new ArrayList<Genre>();
		Studio new_studio = null;
		List<Actor> new_actor_list = new ArrayList<Actor>();
		Director new_director = null;
		List<Movie> new_sequel_list = new ArrayList<Movie>();
		
		List<Object> new_records_list;
		List<Object> new_records = null;
		String new_movie_fk, read;
		
		System.out.println("\nEnter the ID of the movie record you want to update: ");
		movie_id = reader.nextInt();
		reader.nextLine();	// burn the new line of submission
		update_movie = (Movie) EntityService.findRecord("Movie", movie_id);
		
		if(update_movie != null) {
			
			System.out.println("\nEnter the new title of the movie (previus is " + update_movie.getTitle() +" / n for no change): ");
			new_movie_title = reader.nextLine();
			if(new_movie_title.equals("n"))
				new_movie_title = update_movie.getTitle();
			else if(new_movie_title.equals(""))
				new_movie_title = null;
			
			System.out.println("Enter the new runtime of the movie (previus is " + update_movie.getRuntime() +" / -1 for no change):");
			new_movie_runtime = reader.nextInt();
			reader.nextLine();
			if(new_movie_runtime != -1)
				new_movie_runtime = update_movie.getRuntime();
			
			System.out.println("Enter the new date of the movie (previus is " + update_movie.getReleaseDate() +" / n for no change): ");
			String read_date = reader.nextLine();			
			if(read_date.equals("n"))
				new_movie_release_date = update_movie.getReleaseDate();
			else if(read_date.equals(""))
				new_movie_release_date = null;
			else
				new_movie_release_date = new SimpleDateFormat("dd/MM/yyyy").parse(read_date);
			
			if(update_movie.getOverview().length() >= 30)
				new_movie_overview = update_movie.getOverview().substring(0, 30) + "...";
			else
				new_movie_overview = update_movie.getOverview();
			System.out.println("Enter the new overview of the movie (previus is " + new_movie_overview +" / n for no change):");
			new_movie_overview = reader.nextLine();
			if(new_movie_overview.equals("n"))
				new_movie_overview = update_movie.getOverview();
			else if(new_movie_overview.equals(""))
				new_movie_overview = null;
			
			// Update Genres
			new_records_list = EntityService.displayRecords("Genre");
			if(update_movie.getGenres().size() > 0) {
				System.out.print("\nDo you want to change the genres of the movie (previus are {");
				for(Genre genre_record : update_movie.getGenres())
					System.out.print(genre_record.getName() + ", ");
				System.out.print(" } / y to confirm or n for no change)? ");
			}
			else
				System.out.print("\nThere aren't genres for this movie. Do you want to insert them (y/n)? ");
			new_movie_fk = reader.nextLine();
			
			if(!new_movie_fk.equals("n")) {
				update_movie.getGenres().clear();
				do{
					System.out.print("\nEnter a new genre's name of the movie ( ");
					for(Object genre_record : new_records_list)
						System.out.print(((Genre) genre_record).getName() + ", ");
					System.out.println(" ): ");
					new_movie_fk = reader.nextLine();
					
					boolean exist = false;
					for(Object genre_record : new_records_list)
						if(((Genre) genre_record).getName().equalsIgnoreCase(new_movie_fk))
							exist = true;
					
					if(exist) 
					{
						new_records = EntityService.findRecord("FROM Genre WHERE name = \'" + new_movie_fk + "\'");
						new_genre_list.add((Genre) new_records.get(0));
					}
					else
						System.out.println("The genre " + new_movie_fk + " does not exist! Create it!");
					
					System.out.println("\nDoes the movie have another new genre? (y/n): ");
					read = reader.nextLine();
				} while(read.equals("y"));
			}
			new_genre_list.addAll(update_movie.getGenres());

			// Update Studio
			System.out.println("Enter the new studio of the movie (previus is " + update_movie.getStudio().getName() +" / n for no change): ");
			new_movie_fk = reader.nextLine();
			if(!new_movie_fk.equals("n")) {
				new_records_list = EntityService.displayRecords("Studio");

				boolean exist = false;
				for(Object studio_name : new_records_list)
					if(((Studio) studio_name).getName().equalsIgnoreCase(new_movie_fk))
						exist = true;
				
				if(exist) 
				{
					new_records = EntityService.findRecord("FROM Studio WHERE name = \'" + new_movie_fk + "\'");
					new_studio = (Studio) new_records.get(0);
				}
				else
					System.out.println("The studio " + new_movie_fk + " does not exist! Create it!");
			}
			
			// Update Actors
			new_records_list = EntityService.displayRecords("Actor");
			if(update_movie.getActors().size() > 0) {
				System.out.print("\nDo you want to change the actors of the movie (previus are {");
				for(Actor actor_record : update_movie.getActors())
					System.out.print(actor_record.fullName() + ", ");
				System.out.print(" } / y to confirm or n for no change)? ");
			}
			else
				System.out.print("\nThere aren't actors for this movie. Do you want to insert them (y/n)? ");
			new_movie_fk = reader.nextLine();
			
			if(!new_movie_fk.equals("n")) {
				update_movie.getActors().clear();
				do{
					System.out.print("\nEnter a new actor's lastname of the movie ( ");
					for(Object actor_record : new_records_list)
						System.out.print(((Actor) actor_record).fullName() + ", ");
					System.out.println(" ): ");
					new_movie_fk = reader.nextLine();
					
					boolean exist = false;
					for(Object actor_record : new_records_list)
						if(((Actor) actor_record).getLastname().equalsIgnoreCase(new_movie_fk))
							exist = true;
					
					if(exist) 
					{
						new_records = EntityService.findRecord("FROM Actor WHERE lastname = \'" + new_movie_fk + "\'");
						new_actor_list.add((Actor) new_records.get(0));
					}
					else
						System.out.println("The actor " + new_movie_fk + " does not exist! Create it!");
					
					System.out.println("\nDoes the movie have another new actor? (y/n): ");
					read = reader.nextLine();
				} while(read.equals("y"));
			}
			else
				new_actor_list.addAll(update_movie.getActors());
			
			// Update Director
			System.out.println("Enter the new director of the movie (previus is " + update_movie.getDirector().fullName() +" / n for no change): ");
			new_movie_fk = reader.nextLine();
			if(!new_movie_fk.equals("n")) {
				new_records_list = EntityService.displayRecords("Director");

				boolean exist = false;
				for(Object director_name : new_records_list)
					if(((Director) director_name).getLastname().equalsIgnoreCase(new_movie_fk))
						exist = true;
				
				if(exist) 
				{
					new_records = EntityService.findRecord("FROM Director WHERE lastname = \'" + new_movie_fk + "\'");
					new_director = (Director) new_records.get(0);
				}
				else
					System.out.println("The director " + new_movie_fk + " does not exist! Create it!");
			}
			
			// Update Sequels
			new_records_list = EntityService.displayRecords("Movie");
			if(update_movie.getSequels().size() > 0) {
				System.out.print("\nDo you want to change the relation between " + update_movie.getTitle() + " and the other movies (previus are {");
				for(Movie movie_record : update_movie.getSequels())
					System.out.print(movie_record.getTitle() + ", ");
				System.out.print(" } / y to confirm or n for no change)? ");
			}
			else
				System.out.print("\nThere aren't relation between " + update_movie.getTitle() + " and the other movies. Do you want to insert them (y/n)? ");
			new_movie_fk = reader.nextLine();
			
			if(!new_movie_fk.equals("n")) {
				update_movie.getSequels().clear();
				do{
					System.out.print("\nEnter a new movie's title that has relation with " + update_movie.getTitle() + " ( ");
					for(Object movie_record : new_records_list)
						System.out.print(((Movie) movie_record).getTitle() + ", ");
					System.out.println(" ): ");
					new_movie_fk = reader.nextLine();
					
					boolean exist = false;
					for(Object movie_record : new_records_list)
						if(((Movie) movie_record).getTitle().equalsIgnoreCase(new_movie_fk))
							exist = true;
					
					if(exist) 
					{
						new_records = EntityService.findRecord("FROM Movie WHERE title = \'" + new_movie_fk + "\'");
						new_sequel_list.add((Movie) new_records.get(0));
					}
					else
						System.out.println("The movie " + new_movie_fk + " does not exist! Create it!");
					
					System.out.println("\nDoes the movie have another relations? (y/n): ");
					read = reader.nextLine();
				} while(read.equals("y"));
			}
			else
				new_sequel_list.addAll(update_movie.getSequels());
		}
		
		if(new_movie_title != null)
				MovieDao.updateMovie(movie_id, new_movie_title, new_movie_runtime, new_movie_release_date, new_movie_overview, new_genre_list, new_studio, new_actor_list, new_director, new_sequel_list);
		else
			System.out.println("\nThe movie's title can't be null!\nMovie record not created!");
	}
	
	public static void deleteMovie() {
		
		int movie_id;
		
		System.out.println("Enter the ID of the movie record you want to delete: ");
		movie_id = reader.nextInt();
		
		MovieDao.deleteMovie(movie_id);
	}
	
	public static void findMovie() {
		
		int movie_id;
		
		System.out.println("Enter the ID of the movie record you want to find: ");
		movie_id = reader.nextInt();
		
		MovieDao.findMovie(movie_id);
	}
}
