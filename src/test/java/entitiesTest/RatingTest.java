package entitiesTest;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.ActorDao;
import assignment3.dao.DirectorDao;
import assignment3.dao.EntityService;
import assignment3.dao.GenreDao;
import assignment3.dao.MovieDao;
import assignment3.dao.RatingDao;
import assignment3.dao.StudioDao;
import assignment3.dao.UserDao;
import assignment3.entities.*;
import assignment3.entities.Person.Gender;

public class RatingTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    	
    	GenreDao.createGenre("Adventure", "");
		GenreDao.createGenre("Action", "");
		GenreDao.createGenre("Science fiction", "");
		
		StudioDao.createStudio("Universal Pictures", "USA");
		StudioDao.createStudio("Warner Bros.", "USA");

		ActorDao.createActor("Liam", "Hemsworth", new SimpleDateFormat("dd/MM/yyyy").parse("13/01/1990"), Gender.valueOf("MALE"), 15);
		ActorDao.createActor("Jennifer Shrader", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("15/08/1990"), Gender.valueOf("FEMALE") , 15);
		ActorDao.createActor("Joshua Ryan", "Hutcherson", new SimpleDateFormat("dd/MM/yyyy").parse("12/10/1992"), Gender.valueOf("MALE"), 27);
		
		DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);
		DirectorDao.createDirector("Francis", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("26/03/1971"), Gender.valueOf("MALE") , 15);
    
		UserDao.createUser("coich", "p4ssw0rd", new Date());
		UserDao.createUser("vita", "password", new Date());
		UserDao.createUser("il_galimone", "gymBione", new Date());
		
		MovieDao.createMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), null, 
				Arrays.asList(((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0)), 
						((Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0))), 
				((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Warner Bros.\'").get(0)), 
				Arrays.asList(((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0)), 
						((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0))), 
				((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Ross\'").get(0)), null);
    }
    
    private int CreateRating(String username, String movie_title, float vote, Date date_vote, String comment) {
    	User user = new User();
    	user = (User) EntityService.findRecord("FROM User WHERE username = \'" + username + "\'").get(0);
    	Movie movie = new Movie();
    	movie = (Movie) EntityService.findRecord("FROM Movie WHERE title = \'" + movie_title + "\'").get(0);
    	
		return RatingDao.createRating(user, movie, vote, date_vote, comment);
	}
    
	@Test
	public void Create() {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		
		int id_rating = CreateRating("vita", "The Hunger Games", 7.8f, date, "Super!");
		Rating get_rating = RatingDao.findRating(id_rating);
		
		assertEquals(get_rating.getUser().toString(), ((User) EntityService.findRecord("FROM User WHERE username = \'vita\'").get(0)).toString());
		assertEquals(get_rating.getMovie().getTitle(), ((Movie) EntityService.findRecord("FROM Movie WHERE title = \'The Hunger Games\'").get(0)).getTitle());
		assertEquals(get_rating.getVote(), 7.8f, 0f);
		assertEquals(get_rating.getDateVoteString(), df.format(date));
		assertEquals(get_rating.getComment(), "Super!");
	}
	
    @Test
    public void Read() {
    	int id_rating = CreateRating("vita", "The Hunger Games", 7.8f, new Date(), "Super!");
        
        Rating get_rating = RatingDao.findRating(id_rating);
        assertNotNull(get_rating);
    }
	
    @Test
    public void Update() throws ParseException {
    	int id_rating = CreateRating("vita", "The Hunger Games", 1.8f, new Date(), "Super!");
    	Rating get_rating = RatingDao.findRating(id_rating);
    	
    	RatingDao.updateRating(id_rating, get_rating.getUser(), get_rating.getMovie(), 7.2f, new SimpleDateFormat("dd/MM/yyyy").parse("25/12/2017"), "Super!");
        Rating new_rating = RatingDao.findRating(id_rating);
        assertEquals(new_rating.getVote(), 7.2f, 0f);
        assertNotNull(new_rating.getDateVote());
    }
    
    @Test
    public void Delete() {
    	int id_rating = CreateRating("vita", "The Hunger Games", 7.8f, new Date(), "Super!");

    	RatingDao.deleteRating(id_rating);
        Rating get_rating = RatingDao.findRating(id_rating);
        assertNull(get_rating);
    }
    
    @Test
    public void Retrive() {
    	CreateRating("vita", "The Hunger Games", 3.8f, new Date(), "Not my film!");
    	CreateRating("coich", "The Hunger Games", 8.4f, new Date(), "Amazing!");
    	
    	List<Object> ratings_list = EntityService.displayRecords("Rating");
    	assertEquals(ratings_list.size(), 2);
    }
	
    @Test
    public void DeleteAll() {
    	CreateRating("vita", "The Hunger Games", 3.8f, new Date(), "Not my film!");
    	CreateRating("coich", "The Hunger Games", 8.4f, new Date(), "Amazing!");
    	CreateRating("il_galimone", "The Hunger Games", 4.8f, new Date(), "Awful!");

    	RatingDao.deleteAllRatings();
    	List<Object> ratings_list = EntityService.displayRecords("Rating");
    	assertTrue(ratings_list.isEmpty());
    }
    
    @AfterClass
    public static void After() {
    	RatingDao.deleteAllRatings();
    }
}
