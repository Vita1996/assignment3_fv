package entitiesTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.EntityService;
import assignment3.dao.StudioDao;
import assignment3.entities.*;

public class StudioTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    }

	@Test
	public void Create() {
		
		int id_studio = StudioDao.createStudio("Marvel Studios", "USA");
		Studio get_studio = StudioDao.findStudio(id_studio);
		
		assertEquals(get_studio.getName(), "Marvel Studios");
		assertEquals(get_studio.getNation(), "USA");
	}
	
	@Test
    public void Read() {
    	int id_studio = StudioDao.createStudio("Marvel Studios", "USA");

        Studio get_studio = StudioDao.findStudio(id_studio);
        assertNotNull(get_studio);
    }

    @Test
    public void Update() {
    	int id_studio = StudioDao.createStudio("Marvel Studios", null);

    	StudioDao.updateStudio(id_studio, "Marvel Studios", "USA");
        Studio new_studio = StudioDao.findStudio(id_studio);
        assertEquals(new_studio.getNation(), "USA");
    }
    
    @Test
    public void Delete() {
    	int id_studio = StudioDao.createStudio("Marvel Studios", "USA");

    	StudioDao.deleteStudio(id_studio);
        Studio get_studio = StudioDao.findStudio(id_studio);
        assertNull(get_studio);
    }
    
    @Test
    public void Retrive() {
    	StudioDao.createStudio("Marvel Studios", "USA");
		StudioDao.createStudio("20th Century Fox", "USA");
		StudioDao.createStudio("Warner Bros.", null);

    	List<Object> studios_list = EntityService.displayRecords("Studio");
    	assertEquals(studios_list.size(), 3);
    }

    @Test
    public void DeleteAll() {
    	StudioDao.createStudio("Marvel Studios", "USA");
		StudioDao.createStudio("20th Century Fox", "USA");
		StudioDao.createStudio("Warner Bros.", null);

    	StudioDao.deleteAllStudios();
    	List<Object> studios_list = EntityService.displayRecords("Studio");
    	assertTrue(studios_list.isEmpty());
    }
    
    @AfterClass
    public static void After() throws ParseException {
    	StudioDao.deleteAllStudios();
    }
}
