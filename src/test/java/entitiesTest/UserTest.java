package entitiesTest;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.EntityService;
import assignment3.dao.UserDao;
import assignment3.entities.*;

public class UserTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    }

	@Test
	public void Create() throws ParseException {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		
		String id_user = UserDao.createUser("vita", "password", date);
		User get_user = UserDao.findUser(id_user);
		
		assertEquals(get_user.getUsername(), "vita");
		assertEquals(get_user.getPassword(), "password");
		assertEquals(get_user.getRegistrationDateString(), df.format(date));
	}
	
	@Test
    public void Read() {
    	String id_user = UserDao.createUser("vita", "password", new Date());
    	
    	User get_user = UserDao.findUser(id_user);
        assertNotNull(get_user);
    }

    @Test
    public void Update() {
    	String id_user = UserDao.createUser("vita", "password_25", new Date());

    	UserDao.updateUser(id_user, "password", new Date());
        User new_user = UserDao.findUser(id_user);
        assertEquals(new_user.getPassword(), "password");
    }
    
    @Test
    public void Delete() {
    	String id_user = UserDao.createUser("vita", "password", new Date());
    	
    	UserDao.deleteUser(id_user);
        User get_user = UserDao.findUser(id_user);
        assertNull(get_user);
    }
    
    @Test
    public void Retrive() {
    	UserDao.createUser("coich", "p4ssw0rd", new Date());
		UserDao.createUser("vita", "password", new Date());
		UserDao.createUser("user3", "prova", new Date());

    	List<Object> users_list = EntityService.displayRecords("User");
    	assertEquals(users_list.size(), 3);
    }

    @Test
    public void DeleteAll() {
    	UserDao.createUser("coich", "p4ssw0rd", new Date());
		UserDao.createUser("vita", "password", new Date());
		UserDao.createUser("user3", "prova", new Date());

    	UserDao.deleteAllUsers();
    	List<Object> users_list = EntityService.displayRecords("User");
    	assertTrue(users_list.isEmpty());
    }
    
    @AfterClass
    public static void After() throws ParseException {
    	UserDao.deleteAllUsers();
    }
}
