package entitiesTest;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.EntityService;
import assignment3.dao.DirectorDao;
import assignment3.entities.*;
import assignment3.entities.Person.Gender;

public class DirectorTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    }

	@Test
	public void Create() throws ParseException {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		
		int id_director = DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);

		Director get_director = DirectorDao.findDirector(id_director);
		
		assertEquals(get_director.getFirstname(), "Gary");
		assertEquals(get_director.getLastname(), "Ross");
		assertEquals(get_director.getBirthdateString(), df.format(new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956")));
		assertEquals(get_director.getGender(), Gender.valueOf("MALE"));
		assertEquals(get_director.getFilm_directed(), 22);
	}
	
	@Test
    public void Read() throws ParseException {
    	int id_director = DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);

        Director get_director = DirectorDao.findDirector(id_director);
        assertNotNull(get_director);
    }

    @Test
    public void Update() throws ParseException {
    	int id_director = DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 0);
    	
    	DirectorDao.updateDirector(id_director, "Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);
        Director new_director = DirectorDao.findDirector(id_director);
        assertEquals(new_director.getFilm_directed(), 22);
    }
    
    @Test
    public void Delete() throws ParseException {
    	int id_director = DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);

    	DirectorDao.deleteDirector(id_director);
        Director get_director = DirectorDao.findDirector(id_director);
        assertNull(get_director);
    }
    
    @Test
    public void Retrive() throws ParseException {
    	DirectorDao.createDirector("Jon", "Favreau", new SimpleDateFormat("dd/MM/yyyy").parse("19/10/1966"), Gender.valueOf("MALE") , 9);
		DirectorDao.createDirector("Shane", "Black", new SimpleDateFormat("dd/MM/yyyy").parse("16/12/1961"), Gender.valueOf("MALE") , 13);
		DirectorDao.createDirector("James", "Cameron", new SimpleDateFormat("dd/MM/yyyy").parse("16/08/1954"), Gender.valueOf("MALE") , 10);

    	List<Object> directors_list = EntityService.displayRecords("Director");
    	assertEquals(directors_list.size(), 3);
    }

    @Test
    public void DeleteAll() throws ParseException {
    	DirectorDao.createDirector("Jon", "Favreau", new SimpleDateFormat("dd/MM/yyyy").parse("19/10/1966"), Gender.valueOf("MALE") , 9);
		DirectorDao.createDirector("Shane", "Black", new SimpleDateFormat("dd/MM/yyyy").parse("16/12/1961"), Gender.valueOf("MALE") , 13);
		DirectorDao.createDirector("James", "Cameron", new SimpleDateFormat("dd/MM/yyyy").parse("16/08/1954"), Gender.valueOf("MALE") , 10);

    	DirectorDao.deleteAllDirectors();
    	List<Object> directors_list = EntityService.displayRecords("Director");
    	assertTrue(directors_list.isEmpty());
    }
    
    @AfterClass
    public static void After() throws ParseException {
    	DirectorDao.deleteAllDirectors();
    }
}