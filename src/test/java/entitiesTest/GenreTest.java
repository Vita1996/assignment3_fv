package entitiesTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.EntityService;
import assignment3.dao.GenreDao;
import assignment3.entities.*;

public class GenreTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    }

	@Test
	public void Create() {
		
		int id_genre = GenreDao.createGenre("Action", null);
		Genre get_genre = GenreDao.findGenre(id_genre);
		
		assertEquals(get_genre.getName(), "Action");
		assertEquals(get_genre.getDescription(), null);
	}
	
	@Test
    public void Read() {
    	int id_genre = GenreDao.createGenre("Adventure", "Genre that typically use their action scenes to display and explore exotic locations in an energetic way");

        Genre get_genre = GenreDao.findGenre(id_genre);
        assertNotNull(get_genre);
    }

    @Test
    public void Update() {
    	int id_genre = GenreDao.createGenre("Adventure", "");

    	GenreDao.updateGenre(id_genre, "Action", "");
        Genre new_genre = GenreDao.findGenre(id_genre);
        assertEquals(new_genre.getName(), "Action");
    }
    
    @Test
    public void Delete() {
    	int id_genre = GenreDao.createGenre("Adventure", "Genre that typically use their action scenes to display and explore exotic locations in an energetic way");

    	GenreDao.deleteGenre(id_genre);
        Genre get_genre = GenreDao.findGenre(id_genre);
        assertNull(get_genre);
    }
    
    @Test
    public void Retrive() {
    	GenreDao.createGenre("Science fiction", "Genre that uses speculative, fictional science-based depictions of phenomena that are not fully accepted by mainstream science");
    	GenreDao.createGenre("Anime", "");
    	GenreDao.createGenre("Cartoon", null);

    	List<Object> genres_list = EntityService.displayRecords("Genre");
    	assertEquals(genres_list.size(), 3);
    }

    @Test
    public void DeleteAll() {
    	GenreDao.createGenre("Science fiction", "Genre that uses speculative, fictional science-based depictions of phenomena that are not fully accepted by mainstream science");
    	GenreDao.createGenre("Anime", "");
    	GenreDao.createGenre("Cartoon", null);

    	GenreDao.deleteAllGenres();
    	List<Object> genres_list = EntityService.displayRecords("Genre");
    	assertTrue(genres_list.isEmpty());
    }
    
    @AfterClass
    public static void After() throws ParseException {
    	GenreDao.deleteAllGenres();
    }
}
