package entitiesTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)

@Suite.SuiteClasses({

        ActorTest.class,
        DirectorTest.class,
        GenreTest.class,
        MovieTest.class,
        RatingTest.class,
        StudioTest.class,
        UserTest.class

})


public class executeTest {

}
