package entitiesTest;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.ActorDao;
import assignment3.dao.DirectorDao;
import assignment3.dao.EntityService;
import assignment3.dao.GenreDao;
import assignment3.dao.MovieDao;
import assignment3.dao.StudioDao;
import assignment3.entities.*;
import assignment3.entities.Person.Gender;

public class MovieTest {
	
	@Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    	
    	GenreDao.createGenre("Adventure", "");
		GenreDao.createGenre("Action", "");
		GenreDao.createGenre("Science fiction", "");
		
		StudioDao.createStudio("Universal Pictures", "USA");
		StudioDao.createStudio("Warner Bros.", "USA");

		ActorDao.createActor("Liam", "Hemsworth", new SimpleDateFormat("dd/MM/yyyy").parse("13/01/1990"), Gender.valueOf("MALE"), 15);
		ActorDao.createActor("Jennifer Shrader", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("15/08/1990"), Gender.valueOf("FEMALE") , 15);
		ActorDao.createActor("Joshua Ryan", "Hutcherson", new SimpleDateFormat("dd/MM/yyyy").parse("12/10/1992"), Gender.valueOf("MALE"), 27);
		
		DirectorDao.createDirector("Gary", "Ross", new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1956"), Gender.valueOf("MALE") , 22);
		DirectorDao.createDirector("Francis", "Lawrence", new SimpleDateFormat("dd/MM/yyyy").parse("26/03/1971"), Gender.valueOf("MALE") , 15);
    }
	
	private int CreateMovie(String title, int runtime, Date release_date, String overview, List<String> genre_names, String studio_name, List<String> actor_lastnames, String director_lastname, List<String> sequel_titles) throws ParseException {
		
		List<Genre> genres_list = new ArrayList<Genre>();
		if(genre_names != null)
			for(String genre_name : genre_names)
				genres_list.add((Genre) EntityService.findRecord("FROM Genre WHERE name = \'" + genre_name + "\'").get(0));
		
		Studio studio = new Studio();
		studio = (Studio) EntityService.findRecord("FROM Studio WHERE name = \'" + studio_name + "\'").get(0);
		
		List<Actor> actors_list = new ArrayList<Actor>();
		if(actor_lastnames != null)
			for(String actor_lastname : actor_lastnames)
				actors_list.add((Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'" + actor_lastname + "\'").get(0));
		
		Director director = new Director();
		director = (Director) EntityService.findRecord("FROM Director WHERE lastname = \'" + director_lastname + "\'").get(0);
		
		List<Movie> sequels_list = new ArrayList<Movie>();
		if(sequel_titles != null)
			for(String sequel_title : sequel_titles)
				sequels_list.add((Movie) EntityService.findRecord("FROM Movie WHERE title = \'" + sequel_title + "\'").get(0));
		
		return MovieDao.createMovie(title, runtime, release_date, overview, genres_list, studio,
				actors_list, director, sequels_list);
	}

	@Test
	public void Create() throws ParseException {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		
		int id_movie = CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);
		Movie get_movie = MovieDao.findMovie(id_movie);
		
		Set<Genre> genre_set = new HashSet<Genre>();
		Collections.addAll(genre_set, (Genre) EntityService.findRecord("FROM Genre WHERE name = \'Adventure\'").get(0),
				(Genre) EntityService.findRecord("FROM Genre WHERE name = \'Action\'").get(0),
				(Genre) EntityService.findRecord("FROM Genre WHERE name = \'Science fiction\'").get(0));
		
		Set<Actor> actor_set = new HashSet<Actor>();
		Collections.addAll(actor_set, (Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Lawrence\'").get(0), 
				(Actor) EntityService.findRecord("FROM Actor WHERE lastname = \'Hutcherson\'").get(0));
		
		Set<Actor> sequel_set = null;

		assertEquals(get_movie.getTitle(), "The Hunger Games");
		assertEquals(get_movie.getRuntime(), 142);
		assertEquals(get_movie.getReleaseDateString(), df.format(new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012")));
		assertEquals(get_movie.getOverview(), "");
		assertEquals(get_movie.getGenres().size(), genre_set.size());
		assertEquals(get_movie.getStudio().toString(), ((Studio) EntityService.findRecord("FROM Studio WHERE name = \'Warner Bros.\'").get(0)).toString());
		assertEquals(get_movie.getActors().size(), actor_set.size());
		assertEquals(get_movie.getDirector().toString(), ((Director) EntityService.findRecord("FROM Director WHERE lastname = \'Ross\'").get(0)).toString());
		assertNull(sequel_set);
	}
	
	@Test
    public void Read() throws ParseException {
		int id_movie = CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);
		
    	Movie get_movie = MovieDao.findMovie(id_movie);
        assertNotNull(get_movie);
    }

    @Test
    public void Update() throws ParseException {
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	
    	int id_movie = CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);
    	Movie get_movie = MovieDao.findMovie(id_movie);
    	
    	MovieDao.updateMovie(id_movie, "Hunger Games: La ragazza di fuoco", 146, new SimpleDateFormat("dd/MM/yyyy").parse("27/11/2013"), "Sequel of \"The Hunger Games\"", get_movie.getGenres(), get_movie.getStudio(), get_movie.getActors(), get_movie.getDirector(), null);
        Movie new_movie = MovieDao.findMovie(id_movie);
        assertEquals(new_movie.getTitle(), "Hunger Games: La ragazza di fuoco");
        assertEquals(new_movie.getRuntime(), 146);
		assertEquals(new_movie.getReleaseDateString(), df.format(new SimpleDateFormat("dd/MM/yyyy").parse("27/11/2013")));
		assertEquals(new_movie.getOverview(), "Sequel of \"The Hunger Games\"");
    }

    @Test
    public void Delete() throws ParseException {
    	int id_movie = CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);

    	MovieDao.deleteMovie(id_movie);
        Movie get_movie = MovieDao.findMovie(id_movie);
        assertNull(get_movie);
    }
	
    @Test
    public void Retrive() throws ParseException {
    	CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);
    	CreateMovie("Hunger Games: la ragazza di fuoco", 146, new SimpleDateFormat("dd/MM/yyyy").parse("27/11/2013"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Universal Pictures",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", Arrays.asList("The Hunger Games"));
    	CreateMovie("Hunger Games: Il canto della rivolta - Parte 1", 123, new SimpleDateFormat("dd/MM/yyyy").parse("20/11/2014"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Hemsworth", "Lawrence", "Hutcherson"), "Ross", Arrays.asList("The Hunger Games", "Hunger Games: la ragazza di fuoco"));
    	
    	List<Object> movies_list = EntityService.displayRecords("Movie");
    	assertEquals(movies_list.size(), 3);
    }

    @Test
    public void DeleteAll() throws ParseException {
    	CreateMovie("The Hunger Games", 142, new SimpleDateFormat("dd/MM/yyyy").parse("23/03/2012"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", null);
    	CreateMovie("Hunger Games: la ragazza di fuoco", 146, new SimpleDateFormat("dd/MM/yyyy").parse("27/11/2013"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Universal Pictures",
    			Arrays.asList("Lawrence", "Hutcherson"), "Ross", Arrays.asList("The Hunger Games"));
    	CreateMovie("Hunger Games: Il canto della rivolta - Parte 1", 123, new SimpleDateFormat("dd/MM/yyyy").parse("20/11/2014"), "",
    			Arrays.asList("Adventure", "Action", "Science fiction"), "Warner Bros.",
    			Arrays.asList("Hemsworth", "Lawrence", "Hutcherson"), "Ross", Arrays.asList("The Hunger Games", "Hunger Games: la ragazza di fuoco"));
    	
    	MovieDao.deleteAllMovies();
    	List<Object> movies_list = EntityService.displayRecords("Movie");
    	assertTrue(movies_list.isEmpty());
    }
	
    @AfterClass
    public static void After() {
    	MovieDao.deleteAllMovies();
    }
}
