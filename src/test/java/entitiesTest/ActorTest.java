package entitiesTest;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import assignment3.dao.EntityService;
import assignment3.dao.ActorDao;
import assignment3.entities.*;
import assignment3.entities.Person.Gender;

public class ActorTest {
	
    @Before
    public void Before() throws ParseException {
    	EntityService.emptyDB();
    }

	@Test
	public void Create() throws ParseException {
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		
		int id_actor = ActorDao.createActor("Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 30);

		Actor get_actor = ActorDao.findActor(id_actor);
		
		assertEquals(get_actor.getFirstname(), "Robert John");
		assertEquals(get_actor.getLastname(), "Downing Jr.");
		assertEquals(get_actor.getBirthdateString(), df.format(new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965")));
		assertEquals(get_actor.getGender(), Gender.valueOf("MALE"));
		assertEquals(get_actor.getFilm_acted(), 30);
	}
	
	@Test
    public void Read() throws ParseException {
    	int id_actor = ActorDao.createActor("Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 30);

        Actor get_actor = ActorDao.findActor(id_actor);
        assertNotNull(get_actor);
    }

    @Test
    public void Update() throws ParseException {
    	int id_actor = ActorDao.createActor("Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 0);

    	ActorDao.updateActor(id_actor, "Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 30);
        Actor new_actor = ActorDao.findActor(id_actor);
        assertEquals(new_actor.getFilm_acted(), 30);
    }
    
    @Test
    public void Delete() throws ParseException {
    	int id_actor = ActorDao.createActor("Robert John", "Downing Jr.", new SimpleDateFormat("dd/MM/yyyy").parse("04/04/1965"), Gender.valueOf("MALE") , 30);

    	ActorDao.deleteActor(id_actor);
        Actor get_actor = ActorDao.findActor(id_actor);
        assertNull(get_actor);
    }
    
    @Test
    public void Retrive() throws ParseException {
    	ActorDao.createActor("Joshua Ryan", "Hutcherson", new SimpleDateFormat("dd/MM/yyyy").parse("12/10/1992"), Gender.valueOf("MALE"), 27);
		ActorDao.createActor("Liam", "Hemsworth", new SimpleDateFormat("dd/MM/yyyy").parse("13/01/1990"), Gender.valueOf("MALE"), 15);
		ActorDao.createActor("Sam", "Worthington",  new SimpleDateFormat("dd/MM/yyyy").parse("02/08/1976"), Gender.valueOf("MALE"), 32);

    	List<Object> actors_list = EntityService.displayRecords("Actor");
    	assertEquals(actors_list.size(), 3);
    }

    @Test
    public void DeleteAll() throws ParseException {
    	ActorDao.createActor("Joshua Ryan", "Hutcherson", new SimpleDateFormat("dd/MM/yyyy").parse("12/10/1992"), Gender.valueOf("MALE"), 27);
		ActorDao.createActor("Liam", "Hemsworth", new SimpleDateFormat("dd/MM/yyyy").parse("13/01/1990"), Gender.valueOf("MALE"), 15);
		ActorDao.createActor("Sam", "Worthington",  new SimpleDateFormat("dd/MM/yyyy").parse("02/08/1976"), Gender.valueOf("MALE"), 32);

    	ActorDao.deleteAllActors();
    	List<Object> actors_list = EntityService.displayRecords("Actor");
    	assertTrue(actors_list.isEmpty());
    }
    
    @AfterClass
    public static void After() throws ParseException {
    	ActorDao.deleteAllActors();
    }
}